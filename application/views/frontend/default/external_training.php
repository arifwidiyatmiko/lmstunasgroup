<style type="text/css">
    $background1: #c1c1e1;

    html,
    body {
        background-color: $background1;
    }

    table {
        background: white;
    }

    select,
    .select2 {
        display: block;
        width: 100%;
    }

    main {
        margin: 1rem auto;
        max-width: 960px;
    }

    .filters {
        background: darken($background1, 8%);
        border-color: darken($background1, 15%);
    }

    .table-topper {
        background-color: #e1e1e1;

        &:hover,
        &:active {
            background-color: darken(#e1e1e1, 5%);
        }
    }
</style>
<section class="course-header-area">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-xl-12">
                <h1 class="mb-3 header-title"><?php echo get_phrase('Training_eksternal'); ?></h1>

            </div>
        </div>
    </div>
</section>



<section class="course-content-area">
    <div class="container">
        <div class="row align-items-end ">
            <div class="col-xl-8 mb-5">
                <p>Berikut adalah formulir yang dapat digunakan untuk mengajukan training eksternal (diluar dari jadwal atau kalender training yang diadakan oleh People Development Tunas Group). Silahkan diiisi pada setiap bagian yang ada di formulir, dan diwajibkan untuk meminta persetujuan dari atasan langsung. Terima kasih</p>
                <a href="<?= base_url('assets/frontend/Form_Request_Training_2019.pdf') ?>" class="btn btn-block btn-default" target="_blank" rel="noopener noreferrer">Unduh Formulir</a>
            </div>
            <div class="col-xl-4 p-5">
                <form action="<?= base_url('home/external') ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Unggah Berkas</label>
                        <input type="file" accept="" class="form-control" id="file" aria-describedby="fileHelp" name="file" required>
                        <small id="fileHelp" class="form-text text-muted">Unggah berkas yang telah diunduh dan diisi sesuai ketentuan yang tertera. <b>dengan format PDF</b></small>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>