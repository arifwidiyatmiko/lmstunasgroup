<?php

$my_courses = $this->user_model->my_courses()->result_array();

$categories = array();
foreach ($my_courses as $my_course) {
    $course_details = $this->crud_model->get_course_by_id($my_course['course_id'])->row_array();
    if (!in_array($course_details['category_id'], $categories)) {
        array_push($categories, $course_details['category_id']);
    }
}
?>
<section class="page-header-area my-course-area">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="page-title"><?php echo get_phrase('my_courses'); ?></h1>
                <ul>
                    <li><a href="<?php echo site_url('home/my_courses'); ?>"><?php echo get_phrase('my_course'); ?></a></li>
                    <li><a href="<?php echo site_url('home/my_training'); ?>"><?php echo get_phrase('my_training'); ?></a></li>
                    <li><a href="<?php echo site_url('home/my_external'); ?>"><?php echo get_phrase('Training_propose'); ?></a></li>
                    <!-- <li><a href="<?php echo site_url('home/my_wishlist'); ?>"><?php echo get_phrase('wishlists'); ?></a></li> -->
                    <li><a href="<?php echo site_url('home/my_messages'); ?>"><?php echo get_phrase('my_messages'); ?></a></li>
                    <li><a href="<?php echo site_url('home/profile/user_profile'); ?>"><?php echo get_phrase('user_profile'); ?></a></li>
                    <li><a href="<?php echo site_url('home/my_subordinate'); ?>"><?php echo get_phrase('subordinate_progress'); ?></a></li>
                    <li class="active"><a href="<?php echo site_url('home/my_ebook'); ?>"><?php echo get_phrase('my_library'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="my-courses-area">
    <div class="container">

        <!-- <div class="row no-gutters" id="my_courses_area"> -->
        <table id="table-training" class="table table-hover">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($ebook->result_array() as $key => $value) {
                ?>
                    <tr>
                        <td><?= $value['title'] ?></td>
                        <td><?= $value['description'] ?></td>
                        <td><?php 
                            if($value['status_enrol'] == 1){
                                echo "<span class='text-warning'>Mandatory</span>";
                            }else{
                                echo "<span>-</span>";
                            }
                        ?></td>
                        <td>
                            <a class="btn btn-block btn-primary" href="<?= base_url('home/my_ebook/read/' . $value['id']) ?>">Read</a>
                        </td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
        <!-- </div> -->
    </div>
</section>


<script type="text/javascript">
    function getCoursesByCategoryId(category_id) {
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('home/my_courses_by_category'); ?>',
            data: {
                category_id: category_id
            },
            success: function(response) {
                $('#my_courses_area').html(response);
            }
        });
    }

    function getCoursesBySearchString(search_string) {
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('home/my_courses_by_search_string'); ?>',
            data: {
                search_string: search_string
            },
            success: function(response) {
                $('#my_courses_area').html(response);
            }
        });
    }

    function getCourseDetailsForRatingModal(course_id) {
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('home/get_course_details'); ?>',
            data: {
                course_id: course_id
            },
            success: function(response) {
                $('#course_title_1').append(response);
                $('#course_title_2').append(response);
                $('#course_thumbnail_1').attr('src', "<?php echo base_url() . 'uploads/thumbnails/course_thumbnails/'; ?>" + course_id + ".jpg");
                $('#course_thumbnail_2').attr('src', "<?php echo base_url() . 'uploads/thumbnails/course_thumbnails/'; ?>" + course_id + ".jpg");
                $('#course_id_for_rating').val(course_id);
                // $('#instructor_details').text(course_id);
                console.log(response);
            }
        });
    }
    $(document).ready(function() {
        $('#table-training').DataTable();
    });
</script>