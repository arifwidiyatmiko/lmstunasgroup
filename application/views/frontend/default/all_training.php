<?php
$course_details = $this->training_model->get_training($course_id)->row_array();
// $instructor_details = $this->user_model->get_all_user($course_details['user_id'])->row_array();
?>
<section class="">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-xs-12 p-3">
                <div id="calendar"></div>
            </div>
        </div>

    </div>
</section>
<section class="course-carousel-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="course-carousel-title"><?php echo  get_phrase('latest_training'); ?>
                    <a class="float-right" href="<?php echo base_url('home/training/all') ?>">
                        <h4><?php echo  get_phrase('see_more_training'); ?></h4>
                    </a>
                </h2>
            </div>
            <div class="row">
                <?php
                $latest_courses = $this->training_model->get_training()->result_array();
                foreach ($latest_courses as $latest_course) : ?>
                    <div class="col-md-3">
                        <div class="card mb-4 box-shadow">
                            <img class="card-img-top" data-src="<?= base_url('uploads/training/' . $latest_course['thumbnail']) ?>" alt="Thumbnail [100%x225]" style="height: 225px; width: 100%; display: block;" src="<?= base_url('uploads/training/' . $latest_course['thumbnail']) ?>" data-holder-rendered="true">
                            <div class="card-body">
                                <p class="text-center text-bold card-text"><?php echo $latest_course['title']; ?></p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <a href="<?php echo site_url('home/training/' . slugify($latest_course['title']) . '/' . $latest_course['id']); ?>" class="btn btn-sm btn-outline-secondary">Lihat</a>
                                    </div>
                                    <small class="text-muted"><?= date('d M Y h:i', strtotime($latest_course['date'])) ?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="course-box-wrap">
                                    <a href="<?php echo site_url('home/training/' . slugify($latest_course['title']) . '/' . $latest_course['id']); ?>">
                                        <div class="course-box">
                                            <div class="course-image">
                                                <img src="<?= base_url('uploads/training/' . $latest_course['thumbnail']) ?>" alt="" class="img-fluid">
                                            </div>
                                            <div class="course-details">
                                                <h5 class="title"><?php echo $latest_course['title']; ?></h5>
                                                <p class="instructors">
                                                    <?php
                                                    echo $latest_course['description']; ?>
                                                </p>

                                            </div>
                                        </div>
                                    </a>
                                </div> -->
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>

<style media="screen">
    .embed-responsive-16by9::before {
        padding-top: 0px;
    }
</style>
<script type="text/javascript">
    function handleCartItems(elem) {
        url1 = '<?php echo site_url('home/handleCartItems'); ?>';
        url2 = '<?php echo site_url('home/refreshWishList'); ?>';
        $.ajax({
            url: url1,
            type: 'POST',
            data: {
                course_id: elem.id
            },
            success: function(response) {
                $('#cart_items').html(response);
                if ($(elem).hasClass('addedToCart')) {
                    $(elem).removeClass('addedToCart')
                    $(elem).text("<?php echo get_phrase('add_to_cart'); ?>");
                } else {
                    $(elem).addClass('addedToCart')
                    $(elem).text("<?php echo get_phrase('added_to_cart'); ?>");
                }
                $.ajax({
                    url: url2,
                    type: 'POST',
                    success: function(response) {
                        $('#wishlist_items').html(response);
                    }
                });
            }
        });
    }

    function handleBuyNow(elem) {

        url1 = '<?php echo site_url('home/handleCartItemForBuyNowButton'); ?>';
        url2 = '<?php echo site_url('home/refreshWishList'); ?>';
        urlToRedirect = '<?php echo site_url('home/shopping_cart'); ?>';
        var explodedArray = elem.id.split("_");
        var course_id = explodedArray[1];

        $.ajax({
            url: url1,
            type: 'POST',
            data: {
                course_id: course_id
            },
            success: function(response) {
                $('#cart_items').html(response);
                $.ajax({
                    url: url2,
                    type: 'POST',
                    success: function(response) {
                        $('#wishlist_items').html(response);
                        toastr.warning('<?php echo get_phrase('please_wait') . '....'; ?>');
                        setTimeout(
                            function() {
                                window.location.replace(urlToRedirect);
                            }, 1500);
                    }
                });
            }
        });
    }

    function handleEnrolledButton() {
        console.log('here');
        $.ajax({
            url: '<?php echo site_url('home/isLoggedIn'); ?>',
            success: function(response) {
                if (!response) {
                    window.location.replace("<?php echo site_url('login'); ?>");
                }
            }
        });
    }

    function pausePreview() {
        player.pause();
    }

    function slugify(text) {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(/[^\w\-]+/g, '') // Remove all non-word chars
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, ''); // Trim - from end of text
    }
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: ['dayGrid'],
            displayEventTime: false,
            events: <?= json_encode($trainings) ?>,
            eventClick: function(info) {
                let url = '<?= base_url(); ?>home/training/' + slugify(info.event.title);
                url += '/' + info.event.id;
                console.log('Event: ' + url);
                window.location.href = url;
            }

        });
        calendar.setOption('height', 550);
        calendar.setOption('width', 350);
        calendar.render();
    });
</script>