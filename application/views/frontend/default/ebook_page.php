<?php
// echo $course_id;die();
$course_details = $this->ebook_model->getEbook($course_id)->row_array();
// $galery = $this->training_model->get_images($course_id);
// echo $galery->num_rows();
?>
<section class="course-header-area">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="course-header-wrap">
                    <h1 class="title"><span class="text-light">Book's Title</span> : <?php echo $course_details['title']; ?></h1>
                    <!-- <p><?php echo $course_details['description']; ?></p> -->

                </div>
            </div>
        </div>
    </div>
</section>

<section class="course-content-area">
    <div class="container">
        <div class="row mt-3">
            <div class="col-lg-8">
                <a href="#" data-toggle="modal" data-target="#daftarModal" class="btn btn-block btn-primary">Add to Library</a>
                <div class="what-you-get-box">
                    <div class="what-you-get-title">Description</div>
                    <ul class="what-you-get__items">
                    </ul>
                </div>
                <br>
                <div class="course-curriculum-box">
                    <div class="course-curriculum-title clearfix">
                        <div class=" float-left">
                            <?php echo htmlspecialchars_decode($course_details['description']); ?>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-4">
                <div class="images" style="width:100%;">
                    <img src="<?= base_url($course_details['thumbnail']) ?>" class="img-fluid" width="200px"><br />
                </div>
            </div>
        </div>
    </div>
</section>
<?php
if ($this->session->userdata('user_login') == true) {
?>
    <div class="modal fade" id="daftarModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Book Confirmation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h3>Book's Title :  <b><?= $course_details['title'] ?></b></h3>
                    <p>Are you sure add book to library ? </p>
                    <form action="<?= base_url('User/ebook/add/'. $course_details['id'])?>" id="formlogin" method="post">
                        <!-- <div class="content-box">
                            <div class="basic-group">
                                <div class="form-group">
                                    <label for="login-email"><span class="input-field-icon"><i class="fas fa-envelope"></i></span> Confirm your Email:</label>
                                    <input type="email" class="form-control" name="email" id="login-email" placeholder="Email" value="" required>
                                </div>
                            </div>
                        </div> -->
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" form="formlogin" class="btn">Add to my Library</button>
                </div>
            </div>
        </div>
    </div>
<?php
} else {
?>
    <div class="modal fade" id="daftarModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">You have to login first</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('login/validate_login/user') ?>" id="formlogin" method="post">
                        <div class="content-box">
                            <div class="basic-group">
                                <div class="form-group">
                                    <label for="login-email"><span class="input-field-icon"><i class="fas fa-user"></i></span> NIK:</label>
                                    <input type="text" class="form-control" name="nik" id="login-email" placeholder="NIK" value="" required>
                                </div>
                                <div class="form-group">
                                    <label for="login-password"><span class="input-field-icon"><i class="fas fa-lock"></i></span> Password:</label>
                                    <input type="password" class="form-control" name="password" placeholder="Password" value="" required>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" form="formlogin" class="btn">Login</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php
}
?>

<style media="screen">
    .embed-responsive-16by9::before {
        padding-top: 0px;
    }
</style>
<script type="text/javascript">
    function handleCartItems(elem) {
        url1 = '<?php echo site_url('home/handleCartItems'); ?>';
        url2 = '<?php echo site_url('home/refreshWishList'); ?>';
        $.ajax({
            url: url1,
            type: 'POST',
            data: {
                course_id: elem.id
            },
            success: function(response) {
                $('#cart_items').html(response);
                if ($(elem).hasClass('addedToCart')) {
                    $(elem).removeClass('addedToCart')
                    $(elem).text("<?php echo get_phrase('add_to_cart'); ?>");
                } else {
                    $(elem).addClass('addedToCart')
                    $(elem).text("<?php echo get_phrase('added_to_cart'); ?>");
                }
                $.ajax({
                    url: url2,
                    type: 'POST',
                    success: function(response) {
                        $('#wishlist_items').html(response);
                    }
                });
            }
        });
    }

    function handleBuyNow(elem) {

        url1 = '<?php echo site_url('home/handleCartItemForBuyNowButton'); ?>';
        url2 = '<?php echo site_url('home/refreshWishList'); ?>';
        urlToRedirect = '<?php echo site_url('home/shopping_cart'); ?>';
        var explodedArray = elem.id.split("_");
        var course_id = explodedArray[1];

        $.ajax({
            url: url1,
            type: 'POST',
            data: {
                course_id: course_id
            },
            success: function(response) {
                $('#cart_items').html(response);
                $.ajax({
                    url: url2,
                    type: 'POST',
                    success: function(response) {
                        $('#wishlist_items').html(response);
                        toastr.warning('<?php echo get_phrase('please_wait') . '....'; ?>');
                        setTimeout(
                            function() {
                                window.location.replace(urlToRedirect);
                            }, 1500);
                    }
                });
            }
        });
    }

    function handleEnrolledButton() {
        console.log('here');
        $.ajax({
            url: '<?php echo site_url('home/isLoggedIn'); ?>',
            success: function(response) {
                if (!response) {
                    window.location.replace("<?php echo site_url('login'); ?>");
                }
            }
        });
    }

    function pausePreview() {
        player.pause();
    }
</script>