<?php
$course_details = $this->training_model->get_training($course_id)->row_array();
$galery = $this->training_model->get_images($course_id);
// echo $galery->num_rows();
?>
<section class="course-header-area">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="course-header-wrap">
                    <h1 class="title"><?php echo $course_details['title']; ?></h1>
                    <p class="subtitle">Kuota : <b> <?= $participant->num_rows() ?>/<?php echo $course_details['max_participant']; ?> Peserta</b></p>
                    <p><?php echo $course_details['description']; ?></p>
                    <div class="rating-row">
                        <!-- <span class="course-badge best-seller"><?php echo ucfirst($course_details['level']); ?></span> -->

                        <!-- <span class="d-inline-block average-rating"><?php echo $average_ceil_rating; ?></span><span>(<?php echo $number_of_ratings . ' ' . get_phrase('ratings'); ?>)</span> -->
                        <!-- <span class="enrolled-num">
                            <?php
                            $number_of_enrolments = $this->crud_model->enrol_history($course_details['id'])->num_rows();
                            echo $number_of_enrolments . ' ' . get_phrase('students_enrolled');
                            ?>
                        </span> -->

                        <p class="subtitle">Diadakan pada tanggal : <b><?php echo date('D, d M Y', strtotime($course_details['date'])); ?></b></p>
                    </div>
                    <div class="created-row">
                        <!-- <span class="created-by">
                            <?php echo get_phrase('created_by'); ?>
                            <a href="<?php echo site_url('home/instructor_page/' . $course_details['user_id']); ?>"><?php echo $instructor_details['first_name'] . ' ' . $instructor_details['last_name']; ?></a>
                        </span> -->
                        <?php if ($course_details['updated_at'] > 0) : ?>
                            <span class="last-updated-date"><?php echo get_phrase('last_updated') . ' ' . date('D, d-M-Y', strtotime($course_details['updated_at'])); ?></span>
                        <?php else : ?>
                            <span class="last-updated-date"><?php echo get_phrase('last_updated') . ' ' . date('D, d-M-Y', strtotime($course_details['created_at'])); ?></span>
                        <?php endif; ?>
                        <!-- <span class="comment"><i class="fas fa-comment"></i><?php echo ucfirst($course_details['language']); ?></span> -->
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="images" style="width:100%;">
                    <img src="<?= base_url('uploads/training/' . $course_details['thumbnail']) ?>" class="img-fluid"><br />
                </div>
                <div style="margin-top:20px;">
                    <?php
                    $date1 = strtotime($course_details['date']);
                    $date2 = strtotime(date('Y-m-d H:i:s'));
                    $diff = abs($date2 - $date1);
                    $years = floor($diff / (365 * 60 * 60 * 24));
                    $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                    $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
                    if ($days > 3) {
                    ?><a href="#" data-toggle="modal" data-target="#daftarModal" class="btn btn-block btn-primary">Daftar Sekarang</a><?php
                                                                                                                                    } else {
                                                                                                                                        ?><a href="#" class="btn btn-block btn-disabled disabled">Pendaftaran Sudah ditutup</a><?php
                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="course-content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">

                <div class="what-you-get-box">
                    <div class="what-you-get-title">What will i learn?</div>
                    <ul class="what-you-get__items">
                    </ul>
                </div>
                <br>
                <div class="course-curriculum-box">
                    <div class="course-curriculum-title clearfix">
                        <div class=" float-left">
                            <?php echo htmlspecialchars_decode($course_details['goals']); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <?php
        if ($galery->num_rows() > 0) {
        ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="what-you-get-box">
                        <div class="what-you-get-title">Galeri Training</div>
                        <div class="row">
                            <?php
                            foreach ($galery->result_array() as $key => $value) {
                            ?>
                                <div class="col-lg-4">
                                    <div class="card mb-4 box-shadow">
                                        <img class="card-img-top" data-src="<?= base_url('uploads/training/' . $value['path']) ?>" alt="Thumbnail [100%x225]" style="height: 225px; width: 100%; display: block;" src="<?= base_url('uploads/training/' . $value['path']) ?>" data-holder-rendered="true">
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>

                </div>
            </div>
        <?php
        }
        ?>
    </div>
</section>
<?php
if ($this->session->userdata('user_login') == true) {
?>
    <div class="modal fade" id="daftarModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Training Enrollment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h3> <b><?= $course_details['title'] ?></b></h3>
                    <p>Held On : <b><?= $course_details['date'] ?></b></p>
                    <p>Participant : <b><?= $participant->num_rows() . "/" . $course_details['max_participant'] ?></b></p>
                    <!-- <form action="http://localhost/learning/User/enroll/<?= $course_details['id'] ?>" id="formlogin" method="post">
                        <div class="content-box">
                            <div class="basic-group">
                                <div class="form-group">
                                    <label for="login-email"><span class="input-field-icon"><i class="fas fa-envelope"></i></span> Confirm your Email:</label>
                                    <input type="email" class="form-control" name="email" id="login-email" placeholder="Email" value="" required>
                                </div>
                            </div>
                        </div>
                    </form> -->
                </div>
                <div class="modal-footer">
                    <a href="<?= site_url('User/enroll/' . $this->uri->segment(4)) ?>" class="btn">Enroll</a>
                </div>
            </div>
        </div>
    </div>
<?php
} else {
?>
    <div class="modal fade" id="daftarModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">You have to login first</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('login/validate_login/user') ?>" id="formlogin" method="post">
                        <div class="content-box">
                            <div class="basic-group">
                                <div class="form-group">
                                    <label for="login-email"><span class="input-field-icon"><i class="fas fa-user"></i></span> NIK:</label>
                                    <input type="text" class="form-control" name="nik" id="login-email" placeholder="NIK" value="" required>
                                </div>
                                <div class="form-group">
                                    <label for="login-password"><span class="input-field-icon"><i class="fas fa-lock"></i></span> Password:</label>
                                    <input type="password" class="form-control" name="password" placeholder="Password" value="" required>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" form="formlogin" class="btn">Login</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php
}
?>

<style media="screen">
    .embed-responsive-16by9::before {
        padding-top: 0px;
    }
</style>
<script type="text/javascript">
    function handleCartItems(elem) {
        url1 = '<?php echo site_url('home/handleCartItems'); ?>';
        url2 = '<?php echo site_url('home/refreshWishList'); ?>';
        $.ajax({
            url: url1,
            type: 'POST',
            data: {
                course_id: elem.id
            },
            success: function(response) {
                $('#cart_items').html(response);
                if ($(elem).hasClass('addedToCart')) {
                    $(elem).removeClass('addedToCart')
                    $(elem).text("<?php echo get_phrase('add_to_cart'); ?>");
                } else {
                    $(elem).addClass('addedToCart')
                    $(elem).text("<?php echo get_phrase('added_to_cart'); ?>");
                }
                $.ajax({
                    url: url2,
                    type: 'POST',
                    success: function(response) {
                        $('#wishlist_items').html(response);
                    }
                });
            }
        });
    }

    function handleBuyNow(elem) {

        url1 = '<?php echo site_url('home/handleCartItemForBuyNowButton'); ?>';
        url2 = '<?php echo site_url('home/refreshWishList'); ?>';
        urlToRedirect = '<?php echo site_url('home/shopping_cart'); ?>';
        var explodedArray = elem.id.split("_");
        var course_id = explodedArray[1];

        $.ajax({
            url: url1,
            type: 'POST',
            data: {
                course_id: course_id
            },
            success: function(response) {
                $('#cart_items').html(response);
                $.ajax({
                    url: url2,
                    type: 'POST',
                    success: function(response) {
                        $('#wishlist_items').html(response);
                        toastr.warning('<?php echo get_phrase('please_wait') . '....'; ?>');
                        setTimeout(
                            function() {
                                window.location.replace(urlToRedirect);
                            }, 1500);
                    }
                });
            }
        });
    }

    function handleEnrolledButton() {
        console.log('here');
        $.ajax({
            url: '<?php echo site_url('home/isLoggedIn'); ?>',
            success: function(response) {
                if (!response) {
                    window.location.replace("<?php echo site_url('login'); ?>");
                }
            }
        });
    }

    function pausePreview() {
        player.pause();
    }
</script>