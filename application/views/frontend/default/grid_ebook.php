<?php
// $course_details = $this->training_model->get_training($course_id)->row_array();
// $instructor_details = $this->user_model->get_all_user($course_details['user_id'])->row_array();
?>
<section class="course-header-area">
    <div class="container">
        <div class="row align-items-end">
            <h1>All Available Training</h1>
        </div>
    </div>
</section>

<section class="course-carousel-area">
    <div class="container">
        <div class="row mt-5">
            <div class="row posts">
                <?php
                // $latest_courses = $this->training_model->get_training()->result_array();
                foreach ($ebook as $latest_course) : ?>
                    <div class="col-md-3">
                        <div class="card mb-4 box-shadow">
                            <img class="card-img-top" data-src="<?= base_url($latest_course['thumbnail']) ?>" alt="Thumbnail [100%x225]" style="height: 225px; width: 100%; display: block;" src="<?= base_url($latest_course['thumbnail']) ?>" data-holder-rendered="true">
                            <div class="card-body">
                                <p class="text-center text-bold card-text"><?php echo $latest_course['title']; ?></p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <a href="<?php echo site_url('home/ebook/' . slugify($latest_course['title']) . '/' . $latest_course['id']); ?>" class="btn btn-sm btn-outline-secondary">Lihat</a>
                                    </div>
                                    <!-- <small class="text-muted"><?= date('d M Y h:i', strtotime($latest_course['date'])) ?></small> -->
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <h3 class="text-center load-more">Scroll atau Tap disini<br /><span>Untuk lebih banyak</span></h3>
    </div>
</section>

<style media="screen">
    .embed-responsive-16by9::before {
        padding-top: 0px;
    }
</style>
<script type="text/javascript">
    function handleCartItems(elem) {
        url1 = '<?php echo site_url('home/handleCartItems'); ?>';
        url2 = '<?php echo site_url('home/refreshWishList'); ?>';
        $.ajax({
            url: url1,
            type: 'POST',
            data: {
                course_id: elem.id
            },
            success: function(response) {
                $('#cart_items').html(response);
                if ($(elem).hasClass('addedToCart')) {
                    $(elem).removeClass('addedToCart')
                    $(elem).text("<?php echo get_phrase('add_to_cart'); ?>");
                } else {
                    $(elem).addClass('addedToCart')
                    $(elem).text("<?php echo get_phrase('added_to_cart'); ?>");
                }
                $.ajax({
                    url: url2,
                    type: 'POST',
                    success: function(response) {
                        $('#wishlist_items').html(response);
                    }
                });
            }
        });
    }

    function handleBuyNow(elem) {

        url1 = '<?php echo site_url('home/handleCartItemForBuyNowButton'); ?>';
        url2 = '<?php echo site_url('home/refreshWishList'); ?>';
        urlToRedirect = '<?php echo site_url('home/shopping_cart'); ?>';
        var explodedArray = elem.id.split("_");
        var course_id = explodedArray[1];

        $.ajax({
            url: url1,
            type: 'POST',
            data: {
                course_id: course_id
            },
            success: function(response) {
                $('#cart_items').html(response);
                $.ajax({
                    url: url2,
                    type: 'POST',
                    success: function(response) {
                        $('#wishlist_items').html(response);
                        toastr.warning('<?php echo get_phrase('please_wait') . '....'; ?>');
                        setTimeout(
                            function() {
                                window.location.replace(urlToRedirect);
                            }, 1500);
                    }
                });
            }
        });
    }

    function handleEnrolledButton() {
        console.log('here');
        $.ajax({
            url: '<?php echo site_url('home/isLoggedIn'); ?>',
            success: function(response) {
                if (!response) {
                    window.location.replace("<?php echo site_url('login'); ?>");
                }
            }
        });
    }

    function pausePreview() {
        player.pause();
    }

    function slugify(text) {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(/[^\w\-]+/g, '') // Remove all non-word chars
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, ''); // Trim - from end of text
    }
    var page = 8;
    let img_url = '<?= base_url() ?>';
    let ebook_url = '<?= base_url('home/ebook/') ?>';
    $('.load-more').on('click', function() {
        $.ajax({
            url: "<?php echo base_url('home/data_ebook') ?>/" + page,
            cache: false,
            dataType: 'json',
            success: function(data) {
                console.log(data);

                if (data.length == 0) {
                    $('.load-more').hide();
                } else {
                    page += 4;
                    var posts = '';
                    data.forEach(element => {
                        posts += '<div class="col-md-3 fadeIn">\
                                <div class="card mb-4 box-shadow">\
                                    <img class="card-img-top" data-src="' + img_url + element.thumbnail + '" alt="Thumbnail [100%x225]" style="height: 225px; width: 100%; display: block;" src="' + img_url + element.thumbnail + '" data-holder-rendered="true">\
                                    <div class="card-body">\
                                        <p class="text-center text-bold card-text">' + element.title + '</p>\
                                        <div class="d-flex justify-content-between align-items-center">\
                                            <div class="btn-group">\
                                                <a href="' + ebook_url + slugify(element.title) + '/' + element.id + '" class="btn btn-sm btn-outline-secondary">Lihat</a>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>'
                    });
                    $(".posts").append(posts);
                    // observer.observe();
                }
                // $("#results").append(html);
            }
        });
    });
    $(window).scroll(function() {
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            $.ajax({
                url: "<?php echo base_url('home/data_ebook') ?>/" + page,
                cache: false,
                dataType: 'json',
                success: function(data) {
                    console.log(data);

                    if (data.length == 0) {
                        $('.load-more').hide();
                    } else {
                        page += 4;
                        var posts = '';
                        data.forEach(element => {
                            posts += '<div class="col-md-3 fadeIn">\
                                <div class="card mb-4 box-shadow">\
                                    <img class="card-img-top" data-src="' + img_url + element.thumbnail + '" alt="Thumbnail [100%x225]" style="height: 225px; width: 100%; display: block;" src="' + img_url + element.thumbnail + '" data-holder-rendered="true">\
                                    <div class="card-body">\
                                        <p class="text-center text-bold card-text">' + element.title + '</p>\
                                        <div class="d-flex justify-content-between align-items-center">\
                                            <div class="btn-group">\
                                                <a href="' + ebook_url + slugify(element.title) + '/' + element.id + '" class="btn btn-sm btn-outline-secondary">Lihat</a>\
                                            </div>\
                                            <small class="text-muted">' + element.date + '</small>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>'
                        });
                        $(".posts").append(posts);
                        // observer.observe();
                    }
                    // $("#results").append(html);
                }
            });
        }
    });
</script>