<style type="text/css">
    $background1: #c1c1e1;

    html,
    body {
        background-color: $background1;
    }

    table {
        background: white;
    }

    select,
    .select2 {
        display: block;
        width: 100%;
    }

    main {
        margin: 1rem auto;
        max-width: 960px;
    }

    .filters {
        background: darken($background1, 8%);
        border-color: darken($background1, 15%);
    }

    .table-topper {
        background-color: #e1e1e1;

        &:hover,
        &:active {
            background-color: darken(#e1e1e1, 5%);
        }
    }
</style>
<section class="course-header-area">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-xl-12">
                <h1 class="mb-3 header-title"><?php echo get_phrase('innovation'); ?></h1>

                <section class="form-inline">
                    <legend class="col-xs-12">Filters</legend>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <label for="tahun">Tahun</label>
                        <select id="tahun" name="tahun">
                            <option value=""></option>
                            <?php if (!empty($filter['year'])) {
                                foreach ($filter['year'] as $key => $v) {
                            ?>
                                    <option value="<?php echo $v['year'] ?>"><?php echo $v['year'] ?></option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <label for="company">Company</label>
                        <select id="company" name="company">
                            <option value=""></option>
                            <?php if (!empty($filter['company'])) {
                                foreach ($filter['company'] as $key => $v) {
                            ?>
                                    <option value="<?php echo $v['company_name'] ?>"><?php echo $v['company_name'] ?></option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <label for="branch">Branch</label>
                        <select id="branch" name="branch">
                            <option value=""></option>
                            <?php if (!empty($filter['branch'])) {
                                foreach ($filter['branch'] as $key => $v) {
                            ?>
                                    <option value="<?php echo $v['company_name'] ?>"><?php echo $v['company_name'] ?></option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </section>


            </div>
        </div>
    </div>
</section>



<section class="course-content-area">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-xl-12">

                <div class="table-responsive-sm mt-4">
                    <table id="datatable" class="table table-responsive table-centered mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo get_phrase('name'); ?></th>
                                <th><?php echo get_phrase('nik'); ?></th>
                                <th><?php echo get_phrase('tahun'); ?></th>
                                <th><?php echo get_phrase('company'); ?></th>
                                <th><?php echo get_phrase('branch'); ?></th>
                                <th><?php echo get_phrase('title'); ?></th>
                                <th><?php echo get_phrase('description'); ?></th>
                                <!-- <th><?php echo get_phrase('submit date'); ?></th> -->
                                <th><?php echo get_phrase('file'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($innovation['innovation'] as $key => $i) : ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo $i['name']; ?></td>
                                    <td><?php echo $i['nik']; ?></td>
                                    <td><?php echo $i['year']; ?></td>
                                    <td><?php echo (empty($i['center']) ? $i['branch_name'] : $i['center']); ?></td>
                                    <td><?php echo (!empty($i['center']) ? $i['branch_name'] : "-"); ?></td>
                                    <td><?php echo $i['title']; ?></td>
                                    <td><?php echo $i['description']; ?></td>
                                    <!-- <td><?php echo $i['add_date']; ?></td> -->
                                    <td><a href="<?= base_url($i['upload_path'])?>">Unduh</a></td>
                                    
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- end col-->
        </div>
    </div>
</section>

<!-- <script type="text/javascript">
   $(document).ready(function() {
    $('#datatable').dataTable();
} );
</script> -->
<link href="https://rawgit.com/select2/select2/master/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://rawgit.com/select2/select2/master/dist/js/select2.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        var dataTable;

        var select2Init = function() {
            $('select').select2({
                //dropdownAutoWidth : true,
                allowClear: true,
                placeholder: "Select Data",
            });
        };

        var dataTableInit = function() {
            dataTable = $('#datatable').dataTable({
                "columnDefs": [{
                    "targets": 3,
                    "type": 'num',
                }, {
                    "targets": 4,
                    "type": 'text',
                }, {
                    "targets": 5,
                    "type": 'text',
                }],
            });
        };

        var dtSearchInit = function() {

            $('#tahun').change(function() {
                dtSearchAction($(this), 3)
            });
            $('#company').change(function() {
                dtSearchAction($(this), 4);
            });
            $('#branch').change(function() {
                dtSearchAction($(this), 5);
            });

        };

        dtSearchAction = function(selector, columnId) {
            var fv = selector.val();
            if ((fv == '') || (fv == null)) {
                dataTable.api().column(columnId).search('', true, false).draw();
            } else {
                dataTable.api().column(columnId).search(fv, true, false).draw();
            }
        };


        $(document).ready(function() {
            select2Init();
            dataTableInit();
            dtSearchInit();
        });

    });
</script>