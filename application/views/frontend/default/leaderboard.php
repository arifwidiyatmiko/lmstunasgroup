<style type="text/css">
    $background1: #c1c1e1;

    html,
    body {
        background-color: $background1;
    }

    table {
        background: white;
    }

    select,
    .select2 {
        display: block;
        width: 100%;
    }

    main {
        margin: 1rem auto;
        max-width: 960px;
    }

    .filters {
        background: darken($background1, 8%);
        border-color: darken($background1, 15%);
    }

    .table-topper {
        background-color: #e1e1e1;

        &:hover,
        &:active {
            background-color: darken(#e1e1e1, 5%);
        }
    }
</style>
<section class="course-header-area">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-xl-12">
                <h1 class="mb-3 header-title"><?php echo get_phrase('innovation'); ?></h1>
                <form class="required-form" action="<?php echo site_url('home/leaderboard'); ?>" method="post" enctype="multipart/form-data">
                <section class="form-inline">
                  <legend class="col-xs-12">Filters</legend>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <label for="course_id">Course</label>
                      <select id="course_id" class="course_id" name="course_id">
                        <option value=""></option>
                        <?php if(!empty($course)){
                              foreach ($course as $key => $v) {
                        ?>
                            <option value="<?php echo $v['id'] ?>" <?php if($v['id'] == $course_select) echo "selected" ?>><?php echo $v['title'] ?></option>
                        <?php    
                            }
                          }
                      ?>
                      </select>
                    </div>
                  
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <label for="lesson_id">Lesson</label>
                      <select id="lesson_id" name="lesson_id" class="lesson_id" <?php if($lesson_select == 0) echo "disabled"; ?>>
                        <?php 
                          if($lesson_select != 0){
                            foreach ($lesson as $key => $v) {
                              if($v['lesson_type'] == "quiz"){
                        ?>
                          <option value="<?php echo $v['id']; ?>" <?php if($v['id'] == $lesson_select) echo "selected" ?>><?php echo $v['title'] ?></option>
                        <?php 
                          }}}else{
                        ?>
                          <option value="">&mdash; No Quiz &mdash;</option>
                        <?php
                          }
                        ?>
                      </select>
                    </div>
                    </br>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                      <br>
                      <input type="submit" class="btn btn-primary" id='tombol-submit'>
                    </div>
                </section>
              </form>
                


            </div>
        </div>
    </div>
</section>



<section class="course-content-area">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-xl-12">
                <div class="table-responsive-sm mt-4">
                <table id="datatable" class="table table-striped table-centered mb-0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo get_phrase('name'); ?></th>
                      <th><?php echo get_phrase('email'); ?></th>
                      <th><?php echo get_phrase('score'); ?></th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                      if(!empty($leaderboard)){
                       foreach ($leaderboard as $key => $l){ ?>
                          <tr>
                              <td><?php echo $key+1; ?></td>
                              <td><?php echo $l['first_name']." ".$l['last_name']; ?></td>
                              <td><?php echo $l['email']; ?></td>
                              <td><?php echo $l['score']; ?></td>
                          </tr>
                      <?php } }?>
                       
                  </tbody>
              </table>
              </div>
            </div><!-- end col-->
        </div>
    </div>
</section>

<!-- <script type="text/javascript">
   $(document).ready(function() {
    $('#datatable').dataTable();
} );
</script> -->
<link href="https://rawgit.com/select2/select2/master/dist/css/select2.min.css" rel="stylesheet"/>
<script src="https://rawgit.com/select2/select2/master/dist/js/select2.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
  
  var dataTable;
  
  var select2Init = function(){
    $('select').select2({
      //dropdownAutoWidth : true,
      allowClear: true,
      placeholder: "Select Data",
    });
  };
  
  var dataTableInit = function(){
    dataTable = $('#datatable').dataTable({});
  };
  
  $('select.course_id').change(function(e) {
      var course_id = $(this).val();
      let url = '<?= base_url('admin/get_lesson') ?>';
      $.post(url, {course_id: course_id}, function(data) {
        $('.lesson_id').empty();
        $('.lesson_id').html(data);
        if (course_id == "") {
          $(this).attr('required', 'required');
          $('.lesson_id').empty();
          $('.lesson_id').html('<option value="">&mdash; No Quiz &mdash;</option>');
          $('.lesson_id').attr('disabled', 'disabled');
        } else {
          var lesson_length = $('.lesson_id > option').length;
          var lesson_value = $('.lesson_id > option:first-child').val();
          if (lesson_length == '1' && lesson_value == '') {
            $('.lesson_id').attr('disabled', 'disabled');
            $('.lesson_id').removeAttr('required');
          } else {
            $('.lesson_id').removeAttr('disabled');
            $('.lesson_id').removeAttr('required');
          }
        }
      });
    });
  
  $(document).ready(function(){
    select2Init();
    dataTableInit();
    //dtSearchInit();
  });

});
</script>