<section class="menu-area">
  <div class="container-xl">
    <div class="row">
      <div class="col">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">

          <ul class="mobile-header-buttons">
            <li><a class="mobile-nav-trigger" href="#mobile-primary-nav">Menu<span></span></a></li>
            <li><a class="mobile-search-trigger" href="#mobile-search">Search<span></span></a></li>
          </ul>

          <a href="<?php echo site_url(''); ?>" class="navbar-brand" href="#"><img src="<?php echo base_url() . 'uploads/system/logo-dark.png'; ?>" alt="" height="35"></a>

          <?php include 'menu.php'; ?>

          <div class="main-nav-wrap">
            <div class="mobile-overlay"></div>

            <ul class="mobile-main-nav">
              <div class="mobile-menu-helper-top"></div>
              <li class="has-children">
                <a href="">
                  <i class="fas fa-th d-inline"></i>
                  <span>Menu</span>
                  <span class="has-sub-category"><i class="fas fa-angle-right"></i></span>
                </a>


                <ul class="category corner-triangle top-left is-hidden">
                  <li class="go-back"><a href=""><i class="fas fa-angle-left"></i>Menu</a></li>
                  <li class="all-category-devided">
                    <a href="<?= base_url() ?>/home/training">
                      <span class="icon"><i class="fa fa-align-justify"></i></span>
                      <span>Training</span>
                    </a>

                  </li>
                  <li class="all-category-devided">
                    <a href="<?= base_url() ?>/home/external">
                      <span class="icon"><i class="fa fa-align-justify"></i></span>
                      <span>External Training</span>
                    </a>

                  </li>
                  <li class="all-category-devided">
                    <a href="<?= base_url() ?>/home/inovation">
                      <span class="icon"><i class="fa fa-align-justify"></i></span>
                      <span>Inovation</span>
                    </a>

                  </li>

                  <li class="all-category-devided">
                    <a href="<?= base_url() ?>/home/ebook/all">
                      <span class="icon"><i class="fa fa-align-justify"></i></span>
                      <span>E-Library</span>
                    </a>

                  </li>
                </ul>
              </li>



              <div class="mobile-menu-helper-bottom"></div>
            </ul>

          </div>

          <form class="inline-form" action="<?php echo site_url('home/search'); ?>" method="get" style="width: 100%;">
            <div class="input-group search-box mobile-search">
              <input type="text" name='query' class="form-control" placeholder="<?php echo get_phrase('search_for_courses'); ?>">
              <div class="input-group-append">
                <button class="btn" type="submit"><i class="fas fa-search"></i></button>
              </div>
            </div>
          </form>

          <?php if ($this->session->userdata('admin_login')) : ?>
            <div class="instructor-box menu-icon-box">
              <div class="icon">
                <a href="<?php echo site_url('admin'); ?>" style="border: 1px solid transparent; margin: 10px 10px; font-size: 14px; width: 100%; border-radius: 0;"><?php echo get_phrase('administrator'); ?></a>
              </div>
            </div>
          <?php endif; ?>

          <span class="signin-box-move-desktop-helper"></span>
          <div class="sign-in-box btn-group">

            <a href="<?php echo site_url('home/login'); ?>" class="btn btn-sign-in"><?php echo get_phrase('log_in'); ?></a>

          </div> <!--  sign-in-box end -->
        </nav>
      </div>
    </div>
  </div>
</section>