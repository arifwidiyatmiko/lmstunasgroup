<?php

$ebook = $ebook->result()[0];
?>
<section class="page-header-area my-course-area">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="page-title"><?php echo $ebook->title; ?></h1>
                <ul>
                    <li><a href="<?php echo site_url('home/my_ebook'); ?>"><?php echo get_phrase('back_to_my_library'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="my-courses-area">
    <div class="container">
        <div class="">
            <a href="<?php echo base_url(). $ebook->path; ?>" id="embedURL"></a>
            <!-- <a href="https://gahp.net/wp-content/uploads/2017/09/sample.pdf" id="embedURL"></a> -->
            <!-- <a href="<?php echo base_url() . 'uploads/ebook/' . $ebook->path; ?>" class="btn btn-sign-up" download style="color: #fff;">
                <i class="fa fa-download" style="font-size: 20px;"></i> <?php echo get_phrase('download') . ' ' . $ebook->title; ?>
            </a> -->
        </div>
    </div>
</section>


<script src="<?= base_url() ?>assets/lessons/js/jquery.gdocsviewer.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#embedURL').gdocsViewer({
            width: '100%',
            height: 500
        });
    });

    function getCoursesByCategoryId(category_id) {
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('home/my_courses_by_category'); ?>',
            data: {
                category_id: category_id
            },
            success: function(response) {
                $('#my_courses_area').html(response);
            }
        });
    }

    function getCoursesBySearchString(search_string) {
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('home/my_courses_by_search_string'); ?>',
            data: {
                search_string: search_string
            },
            success: function(response) {
                $('#my_courses_area').html(response);
            }
        });
    }

    function getCourseDetailsForRatingModal(course_id) {
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('home/get_course_details'); ?>',
            data: {
                course_id: course_id
            },
            success: function(response) {
                $('#course_title_1').append(response);
                $('#course_title_2').append(response);
                $('#course_thumbnail_1').attr('src', "<?php echo base_url() . 'uploads/thumbnails/course_thumbnails/'; ?>" + course_id + ".jpg");
                $('#course_thumbnail_2').attr('src', "<?php echo base_url() . 'uploads/thumbnails/course_thumbnails/'; ?>" + course_id + ".jpg");
                $('#course_id_for_rating').val(course_id);
                // $('#instructor_details').text(course_id);
                console.log(response);
            }
        });
    }
    $(document).ready(function() {
        $('#table-training').DataTable();
    });
</script>