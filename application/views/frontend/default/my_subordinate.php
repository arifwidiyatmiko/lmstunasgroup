<?php

$my_courses = $this->user_model->my_courses()->result_array();

$categories = array();
foreach ($my_courses as $my_course) {
    $course_details = $this->crud_model->get_course_by_id($my_course['course_id'])->row_array();
    if (!in_array($course_details['category_id'], $categories)) {
        array_push($categories, $course_details['category_id']);
    }
}
?>
<section class="page-header-area my-course-area">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="page-title"><?php echo get_phrase('my_courses'); ?></h1>
                <ul>
                    <li><a href="<?php echo site_url('home/my_courses'); ?>"><?php echo get_phrase('my_course'); ?></a></li>
                    <li><a href="<?php echo site_url('home/my_training'); ?>"><?php echo get_phrase('my_training'); ?></a></li>
                    <li><a href="<?php echo site_url('home/my_external'); ?>"><?php echo get_phrase('Training_propose'); ?></a></li>
                    <!-- <li><a href="<?php echo site_url('home/my_wishlist'); ?>"><?php echo get_phrase('wishlists'); ?></a></li> -->
                    <li><a href="<?php echo site_url('home/my_messages'); ?>"><?php echo get_phrase('my_messages'); ?></a></li>
                    <li><a href="<?php echo site_url('home/profile/user_profile'); ?>"><?php echo get_phrase('user_profile'); ?></a></li>
                    <li class="active"><a href="<?php echo site_url('home/my_subordinate'); ?>"><?php echo get_phrase('subordinate_progress'); ?></a></li>
                    <li><a href="<?php echo site_url('home/my_ebook'); ?>"><?php echo get_phrase('my_library'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="my-courses-area">
    <div class="container">
        <h3>Total Subordinate : <b><?= $subordinate->num_rows() ?></b></h3>
        <form action="<?= base_url('home/my_subordinate') ?>" method="get">
            <div class="row mb-10">
                <div class="col-sm-6">
                    <select class="form-control pull-right" name="subordinate" id="subordinate">
                        <?php if (empty($_GET['subordinate'])) : ?>
                            <option selected value="all"> Tampilkan Semua</option>
                            <?php foreach ($subordinate->result() as $arr) : ?>
                                <option value="<?= $arr->id ?>"><?= $arr->first_name . " " . $arr->last_name ?></option>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <option value="all"> Tampilkan Semua</option>
                            <?php foreach ($subordinate->result() as $arr) : ?>
                                <?php if ($_GET['subordinate'] == $arr->id) : ?>
                                    <option value="<?= $arr->id ?>" selected><?= $arr->first_name . " " . $arr->last_name ?></option>
                                <?php else : ?>
                                    <option value="<?= $arr->id ?>"><?= $arr->first_name . " " . $arr->last_name ?></option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="col-sm-3">
                    <select class="form-control pull-right" name="year" id="year">
                        <?php
                        $years = [];
                        foreach ($data->result() as $key => $value) {
                            $years[] = date('Y', $value->enroll_date);
                        }
                        $years = array_unique($years);
                        // echo json_encode($years);die();
                        ?>
                        <?php if (empty($_GET['year'])) : ?>
                            <option selected value="all"> Tampilkan Semua</option>
                            <?php foreach ($years as $key => $value) : ?>
                                <option value="<?= $value ?>"><?= $value ?></option>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <option value="all"> Tampilkan Semua</option>
                            <?php foreach ($years as $key => $value) : ?>
                                <?php if ($_GET['year'] == $value) : ?>
                                    <option value="<?= $value ?>" selected><?= $value ?></option>
                                <?php else : ?>
                                    <option value="<?= $value ?>"><?= $value ?></option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php
                        ?>
                    </select>
                </div>
                <div class="col-sm-3">
                    <button type="submit" id="form-submit" class="btn btn-primary">Show</button>
                </div>
            </div>
        </form>
        <br />

        <!-- <div class="row no-gutters" id="my_courses_area"> -->
        <table id="table-training" class="table table-hover mt-3">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Training Title</th>
                    <th>Progress</th>
                    <th>Mandatory</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data->result_array() as $key => $value) {
                    if (empty($_GET['subordinate']) && empty($_GET['year'])) {
                ?>
                        <tr>
                            <td><?= $value['first_name'] . " " . $value['last_name'] ?></td>
                            <td><?= $value['title'] ?></td>
                            <td><?= ceil(course_progress_admin($value['id'], $value['user_id'])) ?>%</td>
                            <td>
                                <?php
                                if ($value['is_mandatory'] == true) {
                                ?>
                                    <p>Course Wajib</p>
                                <?php
                                } else {
                                ?>
                                    <p>Enroll Mandiri</p>
                                <?php
                                }
                                ?>
                            </td>
                        </tr>
                    <?php
                    } elseif ($_GET['subordinate']  == 'all' && $_GET['year']  == 'all') {
                    ?>
                        <tr>
                            <td><?= $value['first_name'] . " " . $value['last_name'] ?></td>
                            <td><?= $value['title'] ?></td>
                            <td><?= ceil(course_progress_admin($value['id'], $value['user_id'])) ?>%</td>
                            <td>
                                <?php
                                if ($value['is_mandatory'] == true) {
                                ?>
                                    <p>Course Wajib</p>
                                <?php
                                } else {
                                ?>
                                    <p>Enroll Mandiri</p>
                                <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    } elseif ($_GET['subordinate'] == $value['user_id']) {
                        if ($_GET['year'] == date('Y', $value['date_added']) || $_GET['year'] == 'all') {
                        ?>
                            <tr>
                                <td><?= $value['first_name'] . " " . $value['last_name'] ?></td>
                                <td><?= $value['title'] ?></td>
                                <td><?= ceil(course_progress_admin($value['id'], $value['user_id'])) ?>%</td>
                                <td>
                                    <?php
                                    if ($value['is_mandatory'] == true) {
                                    ?>
                                        <p>Course Wajib</p>
                                    <?php
                                    } else {
                                    ?>
                                        <p>Enroll Mandiri</p>
                                    <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                <?php
                        }
                    }
                }
                ?>
            </tbody>
        </table>
        <!-- </div> -->
    </div>
</section>


<script type="text/javascript">
    $(document).ready(function() {

    });

    function getCoursesByCategoryId(category_id) {
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('home/my_courses_by_category'); ?>',
            data: {
                category_id: category_id
            },
            success: function(response) {
                $('#my_courses_area').html(response);
            }
        });
    }

    function getCoursesBySearchString(search_string) {
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('home/my_courses_by_search_string'); ?>',
            data: {
                search_string: search_string
            },
            success: function(response) {
                $('#my_courses_area').html(response);
            }
        });
    }

    function getCourseDetailsForRatingModal(course_id) {
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('home/get_course_details'); ?>',
            data: {
                course_id: course_id
            },
            success: function(response) {
                $('#course_title_1').append(response);
                $('#course_title_2').append(response);
                $('#course_thumbnail_1').attr('src', "<?php echo base_url() . 'uploads/thumbnails/course_thumbnails/'; ?>" + course_id + ".jpg");
                $('#course_thumbnail_2').attr('src', "<?php echo base_url() . 'uploads/thumbnails/course_thumbnails/'; ?>" + course_id + ".jpg");
                $('#course_id_for_rating').val(course_id);
                // $('#instructor_details').text(course_id);
                console.log(response);
            }
        });
    }
    $(document).ready(function() {
        $('#table-training').DataTable();
    });
</script>