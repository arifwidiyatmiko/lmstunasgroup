<!-- bundle -->
<script src="<?php echo base_url('assets/backend/js/app.min.js'); ?>"></script>
<!-- third party js -->
<script src="<?php echo base_url('assets/backend/js/vendor/Chart.bundle.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/jquery-jvectormap-1.2.2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/jquery-jvectormap-world-mill-en.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/dataTables.bootstrap4.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/dataTables.buttons.min.js'); ?>"></script>


<script src="<?php echo base_url('assets/backend/js/vendor/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/vfs_fonts.js'); ?>"></script>

<script src="<?php echo base_url('assets/backend/js/vendor/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/summernote-bs4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/fullcalendar.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/pages/demo.summernote.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/dropzone.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/pages/demo.dashboard.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/pages/datatable-initializer.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/font-awesome-icon-picker/fontawesome-iconpicker.min.js'); ?>" charset="utf-8"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/bootstrap-tagsinput.min.js'); ?>" charset="utf-8"></script>
<script src="<?php echo base_url('assets/backend/js/bootstrap-tagsinput.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/js/vendor/dropzone.min.js'); ?>" charset="utf-8"></script>
<script src="<?php echo base_url('assets/backend/js/ui/component.fileupload.js'); ?>" charset="utf-8"></script>
<script src="<?php echo base_url('assets/backend/js/pages/demo.form-wizard.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/plugins/datepicker/js/bootstrap-datepicker.min.js'); ?>"></script>
<!-- dragula js-->
<script src="<?php echo base_url('assets/backend/js/vendor/dragula.min.js'); ?>"></script>
<!-- component js -->
<script src="<?php echo base_url('assets/backend/js/ui/component.dragula.js'); ?>"></script>

<script src="<?php echo site_url('assets/backend/js/custom.js'); ?>"></script>
<!-- <script src="<?php echo base_url('assets/backend/plugins/ckeditor/ckeditor.js'); ?>"></script> -->
<script src="https://cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>

<!-- Dashboard chart's data is coming from this file -->
<?php include 'admin/dashboard-chart.php'; ?>

<script type="text/javascript">
  Dropzone.autoDiscover = false;
  $(document).ready(function() {
    $(function() {
      $('.icon-picker').iconpicker();
      var foto_upload = new Dropzone(".dropzone", {
        url: "<?php echo base_url('admin/training/galeri_form/' . $this->uri->segment(4)) ?>",
        maxFilesize: 2,
        method: "post",
        acceptedFiles: "image/*",
        paramName: "userfile",
        dictInvalidFileType: "Type file ini tidak dizinkan",
        addRemoveLinks: true,
        init: function() {
          myDropzone = this;
          $.get('<?php echo base_url('admin/training/get_galeri/' . $this->uri->segment(4)) ?>', function(data) {
            data = JSON.parse(data);
            $.each(data, function(key, value) {
              var mockFile = {
                name: value.name,
                size: value.size,
                token: value.token,
              };
              myDropzone.options.addedfile.call(myDropzone, mockFile);
              myDropzone.options.thumbnail.call(myDropzone, mockFile, value.url);
            });
            // console.log(typeof(JSON.parse(data)));

          });
        }
      });

      //Event ketika Memulai mengupload
      foto_upload.on("sending", function(a, b, c) {
        a.token = Math.random();
        c.append("token_foto", a.token); //Menmpersiapkan token untuk masing masing foto
      });


      //Event ketika foto dihapus
      foto_upload.on("removedfile", function(a) {
        var token = a.token;
        console.log(a)
        $.ajax({
          type: "post",
          data: {
            token: token
          },
          url: "<?php echo base_url('admin/training/galeri_delete/' . $this->uri->segment(4)) ?>",
          cache: false,
          dataType: 'json',
          success: function() {
            console.log("Foto terhapus");
          },
          error: function() {
            console.log("Error");

          }
        });
      });
    });
  });
</script>

<!-- Toastr and alert notifications scripts -->
<script type="text/javascript">
  function notify(message) {
    $.NotificationApp.send("<?php echo get_phrase('heads_up'); ?>!", message, "top-right", "rgba(0,0,0,0.2)", "info");
  }

  function success_notify(message) {
    $.NotificationApp.send("<?php echo get_phrase('congratulations'); ?>!", message, "top-right", "rgba(0,0,0,0.2)", "success");
  }

  function error_notify(message) {
    $.NotificationApp.send("<?php echo get_phrase('oh_snap'); ?>!", message, "top-right", "rgba(0,0,0,0.2)", "error");
  }

  function error_required_field() {
    $.NotificationApp.send("<?php echo get_phrase('oh_snap'); ?>!", "<?php echo get_phrase('please_fill_all_the_required_fields'); ?>", "top-right", "rgba(0,0,0,0.2)", "error");
  }

  function error_size_upload() {
    $.NotificationApp.send("<?php echo get_phrase('oh_snap'); ?>!", "<?php echo get_phrase('check_type_file_and_size_file'); ?>", "top-right", "rgba(0,0,0,0.2)", "error");
  }
</script>

<?php if ($this->session->flashdata('info_message') != "") : ?>
  <script type="text/javascript">
    $.NotificationApp.send("<?php echo get_phrase('success'); ?>!", '<?php echo $this->session->flashdata("info_message"); ?>', "top-right", "rgba(0,0,0,0.2)", "info");
  </script>
<?php endif; ?>

<?php if ($this->session->flashdata('error_message') != "") : ?>
  <script type="text/javascript">
    $.NotificationApp.send("<?php echo get_phrase('oh_snap'); ?>!", '<?php echo $this->session->flashdata("error_message"); ?>', "top-right", "rgba(0,0,0,0.2)", "error");
  </script>
<?php endif; ?>

<?php if ($this->session->flashdata('flash_message') != "") : ?>
  <script type="text/javascript">
    $.NotificationApp.send("<?php echo get_phrase('congratulations'); ?>!", '<?php echo $this->session->flashdata("flash_message"); ?>', "top-right", "rgba(0,0,0,0.2)", "success");
  </script>
<?php endif; ?>