<?php
$user_data = $this->db->get_where('users', array('id' => $user_id))->row_array();
$social_links = json_decode($user_data['social_links'], true);
?>
<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo $user->first_name . " " . $user->last_name; ?>' Library
                    <a href="#" data-toggle="modal" data-target="#enrollUser" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i><?php echo get_phrase('Enroll ebook'); ?></a>
                </h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title mb-3"><?php echo get_phrase('List_ebooks'); ?></h4>
                <div class="table-responsive-sm mt-4">
                    <table id="basic-datatable" class="table table-striped table-centered mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo get_phrase('title'); ?></th>
                                <th><?php echo get_phrase('description'); ?></th>
                                <th><?php echo get_phrase('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            // print_r($courses->result());die();
                            foreach ($ebook->result() as $key => $data) : ?>
                                <?php
                                // if ($item->is_mandatory == true) {
                                // $data = $this->crud_model->get_course_by_id($item->course_id)->result()[0];
                                ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?= $data->title ?></td>
                                    <td><?= $data->description ?> </td>
                                    <td>
                                        <a href="#" onclick="confirm_modal('<?= base_url('Admin/users/remove_ebook/' . $user_id . '/' . $data->id) ?>');" class="btn btn-warning"> Remove Courses</a>
                                    </td>
                                </tr>
                                <?php
                                // }
                                ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>


            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div>
</div>
<div class="modal fade" id="enrollUser" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">User Name : <?= $user->first_name ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('admin/ebook/bulk/' . $user->id . '') ?>" id="my-form" method="post">
                    <div class="form-group">
                        <label for="users">Choose Ebook <b>You can choose one or more</b></label>
                        <select multiple="multiple" class="form-control js-example-basic-multiple" name="ebook[]" id="ebook">
                            <?php
                            foreach ($ebooks->result_array() as $key => $value) {
                            ?>
                                    <option value="<?= $value['id'] ?>"><?= $value['title'] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" form="my-form">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
    });
</script>