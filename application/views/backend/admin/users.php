<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo $page_title; ?>
                    <!-- <a href="<?php echo site_url('admin/excel/users'); ?>" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i><?php echo get_phrase('Download Format'); ?></a>&nbsp; -->
                    <div class="btn-group  float-right">
                        <button type="button" class="btn btn-outline-primary btn-rounded alignToTitle dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Import
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#import">Data users</a>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#import_mandatory_course">Mandatory Course</a>
                            <!-- <a class="dropdown-item" href="#" data-toggle="modal" data-target="#import_mandatory_ebook">Mandatory Ebook</a> -->
                        </div>
                    </div>&nbsp;
                    <div class="btn-group  float-right">
                        <button type="button" class="btn btn-outline-primary btn-rounded alignToTitle dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Downlaod Format
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="<?php echo site_url('admin/excel/users'); ?>">Data users</a>
                            <a class="dropdown-item" href="<?php echo site_url('admin/excel/mandatory_course'); ?>">Mandatory Course</a>
                            <!-- <a class="dropdown-item" href="<?php echo site_url('admin/excel/ebook_courses'); ?>">Mandatory Ebook</a> -->
                        </div>
                    </div>&nbsp;
                    <a target="blank" href="<?php echo site_url('admin/user_form/add_user_form'); ?>" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i><?php echo get_phrase('add_users'); ?></a>
                </h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-3 header-title"><?php echo get_phrase('students'); ?></h4>
                <div class="table-responsive-sm mt-4">
                    <table id="basic-datatable" class="table table-striped table-centered mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo get_phrase('NIK'); ?></th>
                                <th><?php echo get_phrase('name'); ?></th>
                                <th><?php echo get_phrase('email'); ?></th>
                                <th><?php echo get_phrase('enrolled_courses'); ?></th>
                                <th><?php echo get_phrase('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($users->result_array() as $key => $user) : ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo $user['nik']; ?></td>
                                    <td><?php echo $user['first_name'] . ' ' . $user['last_name']; ?></td>
                                    <td><?php echo $user['email']; ?></td>
                                    <td>
                                        <?php
                                        $enrolled_courses = $this->crud_model->enrol_history_by_user_id($user['id']);
                                        $enrolled_book = $this->crud_model->enrol_history_by_user_id($user['id']);
                                        ?>
                                        <?= $enrolled_courses->num_rows() ?> Courses
                                    </td>
                                    <td>
                                        <div class="dropright dropright">
                                            <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="mdi mdi-dots-vertical"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item" href="<?php echo site_url('admin/user_form/edit_user_form/' . $user['id']) ?>"><?php echo get_phrase('edit'); ?></a></li>
                                                <li><a class="dropdown-item" href="#" onclick="confirm_modal('<?php echo site_url('admin/users/delete/' . $user['id']); ?>');"><?php echo get_phrase('delete'); ?></a></li>
                                                <li><a class="dropdown-item" href="<?php echo site_url('admin/user_form/courses/' . $user['id']) ?>"><?php echo get_phrase('courses'); ?></a></li>
                                                <li><a class="dropdown-item" href="<?php echo site_url('admin/user_form/ebook/' . $user['id']) ?>"><?php echo get_phrase('Ebook'); ?></a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<div class="modal fade" id="import" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Import Users data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('admin/import/users') ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleFormControlFile1">File Format Excel</label>
                        <input type="file" class="form-control-file" id="excel" name="excel">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="import_mandatory_course" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Import Mandatory Courses</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('admin/import/mandatory_course') ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleFormControlFile1">File Format Excel</label>
                        <input type="file" class="form-control-file" id="excel" name="excel">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>