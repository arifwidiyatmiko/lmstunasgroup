<!-- start page title -->
<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('add_new_category'); ?></h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row justify-content-center">
    <div class="col-xl-7">
        <div class="card">
            <div class="card-body">
                <div class="col-lg-12">
                    <h4 class="mb-3 header-title"><?php echo get_phrase('ebook_edit_form'); ?></h4>

                    <form class="required-form" action="<?php echo site_url('admin/ebook/edit'); ?>" method="post" enctype="multipart/form-data">

                        <input type="hidden" name="id" value="<?= $ebook['ebook'][0]['id'] ?>">
                        <div class="form-group">
                            <label for="title"><?php echo get_phrase('title'); ?></label><span class="required">*</span></label>
                            <input type="text" class="form-control" name="title" value="<?php echo $ebook['ebook'][0]['title']; ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="description"><?php echo get_phrase('description'); ?><span class="required">*</span></label>
                            <textarea cols="80" id="editor-desc" name="description" rows="10" required><?php echo $ebook['ebook'][0]['description']; ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="file-attach"><?php echo get_phrase('thumbnail image'); ?></label></label><span class="required"> (jpg/jpeg/png file, max size 2MB)</span></label>
                            <input type="file" id="thumbnail" name="thumbnail" />
                            <?php if (!empty($ebook['ebook'][0]['thumbnail'])) {
                                $path = $ebook['ebook'][0]['thumbnail'];
                            ?>
                                </br>
                                <a href="<?php echo base_url() . $ebook['ebook'][0]['thumbnail'] ?>" target="_blank">
                                    <i class="dripicons-document"><?php echo substr($path, strrpos($path, '/') + 1) ?></i>
                                </a>
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            <label for="file-attach"><?php echo get_phrase('upload_dokumen'); ?></label></label><span class="required"> (PDF/DOCX file, max size 2MB)</span></label>
                            <input type="file" id="dokumen" name="dokumen" />
                            <?php if (!empty($ebook['ebook'][0]['path'])) {
                                $path = $ebook['ebook'][0]['path'];
                            ?>
                                </br>
                                <a href="<?php echo base_url() . $ebook['ebook'][0]['path'] ?>" target="_blank">
                                    <i class="dripicons-document"><?php echo substr($path, strrpos($path, '/') + 1) ?></i>
                                </a>
                            <?php } ?>
                        </div>
                        <button type="button" class="btn btn-primary" id='tombol-submit' onclick="checkRequiredFields()"><?php echo get_phrase("submit"); ?></button>
                    </form>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<script>
    $(document).ready(function() {
        CKEDITOR.replace('editor-desc');


        $('#dokumen').bind('change', function() {

            //this.files[0].size gets the size of your file.
            // alert(this.files[0].name.split('.').pop().toLowerCase());
            var size = this.files[0].size;
            var ext = this.files[0].name.split('.').pop().toLowerCase();
            if (size > 2097152 || $.inArray(ext, ['pdf']) == -1) {
                error_size_upload();
                $('#tombol-submit').prop('disabled', true);
            } else {
                $('#tombol-submit').prop('disabled', false);
            }

        });

    });
</script>