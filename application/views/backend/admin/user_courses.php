<?php
$user_data = $this->db->get_where('users', array('id' => $user_id))->row_array();
$social_links = json_decode($user_data['social_links'], true);
?>
<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo $user->first_name . " " . $user->last_name; ?>' Courses </h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title mb-3"><?php echo get_phrase('List_enrolled_courses'); ?></h4>
                <div class="table-responsive-sm mt-4">
                    <table id="basic-datatable" class="table table-striped table-centered mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo get_phrase('course_name'); ?></th>
                                <th><?php echo get_phrase('progress'); ?></th>
                                <th>Status</th>
                                <th><?php echo get_phrase('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            // print_r($courses->result());die();
                            foreach ($courses->result() as $key => $item) : ?>
                                <?php
                                // if ($item->is_mandatory == true) {
                                $data = $this->crud_model->get_course_by_id($item->course_id)->result()[0];
                                ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?= $data->title ?></td>
                                    <td><?= ceil(course_progress_admin($data->id, $user_id)) ?>% </td>
                                    <td>
                                        <?php if ($item->is_mandatory == true) : ?>
                                            <p class="text-warning">Mandatory Course</p>
                                        <?php else : ?>
                                            <p class="text-success">Enrolled Course</p>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <a href="#" onclick="confirm_modal('<?= base_url('Admin/users/remove_course/' . $user_id . '/' . $item->course_id) ?>');" class="btn btn-warning"> Remove Courses</a>
                                        <a href="#" onclick="confirm_modal('<?= base_url('Admin/users/reset_course/' . $user_id . '/' . $item->course_id) ?>');" class="btn btn-danger"> Reset Progress</a>
                                    </td>
                                </tr>
                                <?php
                                // }
                                ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>


            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div>
</div>