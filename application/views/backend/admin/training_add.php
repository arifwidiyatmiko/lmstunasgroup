<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo 'Form Training'; ?> </h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<div class="row justify-content-center">
    <div class="col-xl-12">
        <?php
        if ($this->session->flashdata('error')) {
        ?>
            <div class="alert alert-danger" role="alert">
                <?= $this->session->flashdata('error')['error'] ?>
            </div>
        <?php
        }
        ?>
        <div class="card">
            <div class="card-body">
                <div class="col-lg-12">

                    <form class="required-form" action="<?php echo site_url('admin/training/add'); ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="name">Training title<span class="required">*</span></label>
                            <input type="text" class="form-control" id="title" name="title" required="">
                        </div>

                        <div class="form-group">
                            <label for="name">Training Description<span class="required">*</span></label>
                            <textarea name="description" id="description" rows="5" class="form-control" required=""></textarea>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label for="name">Training Date<span class="required">*</span></label>
                                <input type="text" class="form-control datepicker" id="date" name="date" placeholder="dd/mm/yyyy" required="">
                            </div>
                            <div class="col-sm-6 form-group">
                                <label for="name">Maximum Participant<span class="required">*</span></label>
                                <input type="number" min="1" class="form-control" id="max_participant" name="max_participant" required="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label for="name">Category<span class="required">*</span></label>
                                <select name="id_category" id="id_category" class="form-control" required>
                                    <option selected disabled> -- </option>
                                    <?php
                                    foreach ($category as $key => $value) {
                                        echo '<option value="' . $value->id . '">' . $value->name . '</option>';
                                    }
                                    ?>
                                </select>
                                <!-- <input type="number" min="1" class="form-control" id="max_participant" name="max_participant" required=""> -->
                            </div>
                            <div class="form-group" id="thumbnail-picker-area">
                                <label> Category thumbnail <small>(The image size should be: 400 X 255)</small> </label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="thumbnail" name="thumbnail" accept="image/*" onchange="changeTitleOfImageUploader(this)">
                                        <label class="custom-file-label" for="thumbnail">Choose thumbnail</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name">Training Goals<span class="required">*</span></label>
                            <textarea name="goals" id="goals" class="form-control" required=""></textarea>
                        </div>


                        <button type="button" class="btn btn-primary" onclick="checkRequiredFields()">Submit</button>
                    </form>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<script>
    $(document).ready(function() {
        CKEDITOR.replace('goals');
        $('.datepicker').datepicker({
            'format': 'dd/mm/yyyy'
        });
    });
</script>