<div class="row ">
  <div class="col-xl-12">
    <div class="card">
      <div class="card-body">
        <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo $page_title; ?>
        
          <!-- <a href="<?php echo site_url('admin/ebook/add'); ?>" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i><?php echo get_phrase('add_innovation'); ?></a> -->
          <a href="<?php echo site_url('admin/ebook/add'); ?>" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i><?php echo get_phrase('add_ebook'); ?></a>
        </h4>
      </div> <!-- end card body-->
    </div> <!-- end card -->
  </div><!-- end col-->
</div>

<div class="row">
  <div class="col-xl-12">
    <div class="card">
      <div class="card-body">
        <h4 class="mb-3 header-title"><?php echo get_phrase('ebook'); ?></h4>


        <div class="table-responsive-sm mt-4">
          <table id="datatable" class="table table-striped table-centered mb-0">
            <thead>
              <tr>
                <th>#</th>
                <th><?php echo get_phrase('title'); ?></th>
                <th><?php echo get_phrase('thumbnail'); ?></th>
                <th><?php echo get_phrase('description'); ?></th>
                <th><?php echo get_phrase('file'); ?></th>
                <th><?php echo get_phrase('actions'); ?></th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($ebook['ebook'] as $key => $i) : ?>
                <tr>
                  <td><?php echo $key + 1; ?></td>
                  <td><?php echo $i['title']; ?></td>
                  <td><img src="<?= base_url($i['thumbnail']) ?>" width="100px"></td>
                  <td><?php echo $i['description']; ?></td>
                  <td><a href="<?= base_url($i['path']) ?>" target="blank" rel="noopener">Unduh</a></td>
                  <td>
                    <div class="dropright dropright">
                      <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="mdi mdi-dots-vertical"></i>
                      </button>
                      <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="<?php echo site_url('admin/ebook/edit/' . $i['id']) ?>"><?php echo get_phrase('edit'); ?></a></li>
                        <li><a class="dropdown-item" href="#" onclick="confirm_modal('<?php echo site_url('admin/ebook/delete/' . $i['id']); ?>');"><?php echo get_phrase('delete'); ?></a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div> <!-- end card body-->
    </div> <!-- end card -->
  </div><!-- end col-->
</div>

<!-- <script type="text/javascript">
   $(document).ready(function() {
    $('#datatable').dataTable();
} );
</script> -->
<link href="https://rawgit.com/select2/select2/master/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://rawgit.com/select2/select2/master/dist/js/select2.js"></script>
<script type="text/javascript">
  $(document).ready(function() {

    var dataTable;

    var select2Init = function() {
      $('select').select2({
        //dropdownAutoWidth : true,
        allowClear: true,
        placeholder: "Select Data",
      });
    };

    var dataTableInit = function() {
      dataTable = $('#datatable').dataTable({
        "columnDefs": [{
          "targets": 3,
          "type": 'num',
        }, {
          "targets": 4,
          "type": 'text',
        }, {
          "targets": 5,
          "type": 'text',
        }],
      });
    };

    var dtSearchInit = function() {

      $('#tahun').change(function() {
        dtSearchAction($(this), 3)
      });
      $('#company').change(function() {
        dtSearchAction($(this), 4);
      });
      $('#branch').change(function() {
        dtSearchAction($(this), 5);
      });

    };

    dtSearchAction = function(selector, columnId) {
      var fv = selector.val();
      if ((fv == '') || (fv == null)) {
        dataTable.api().column(columnId).search('', true, false).draw();
      } else {
        dataTable.api().column(columnId).search(fv, true, false).draw();
      }
    };


    $(document).ready(function() {
      select2Init();
      dataTableInit();
      dtSearchInit();
    });

  });
</script>