<!-- start page title -->
<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('add_new_company'); ?></h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row justify-content-center">
    <div class="col-xl-7">
        <div class="card">
            <div class="card-body">
              <div class="col-lg-12">
                <h4 class="mb-3 header-title"><?php echo get_phrase('company/branch_add_form'); ?></h4>

                <form class="required-form" action="<?php echo site_url('admin/company/add'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="code"><?php echo get_phrase('company_name'); ?></label><span class="required">*</span></label>
                        <input type="text" class="form-control" name = "company_name" value="" required>
                    </div>

                    <div class="form-group">
                        <label for="company"><?php echo get_phrase('company_center'); ?></label><span class="required">*</span></label>
                        </br>
                        <span class="required">choose company center if you want to input branch or default if you want to input company center </span>
                        <select class="form-control company" name="company_center" required>
                          <option value="0">Company Center</option>
                          <?php foreach ($company as $key => $v) { ?>
                                  <option value="<?php echo $v['id'] ?>"><?php echo $v['company_name'] ?></option>
                          <?php } ?>
                        </select>
                    </div>

                    <button type="button" class="btn btn-primary" id='tombol-submit' onclick="checkRequiredFields()"><?php echo get_phrase("submit"); ?></button>
                </form>
              </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

