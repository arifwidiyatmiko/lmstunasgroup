<!-- start page title -->
<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('add_new_category'); ?></h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row justify-content-center">
    <div class="col-xl-7">
        <div class="card">
            <div class="card-body">
                <div class="col-lg-12">
                    <h4 class="mb-3 header-title"><?php echo get_phrase('innovation_add_form'); ?></h4>

                    <form class="required-form" action="<?php echo site_url('admin/innovation/add'); ?>" method="post" enctype="multipart/form-data">

                        <div class="form-group">
                            <label for="code"><?php echo get_phrase('Type'); ?></label><span class="required">*</span></label>
                            <!-- <input type="text" class="form-control" name="type" id="" value="" required> -->
                            <select class="form-control" name="type" id="type" required>
                                <option selected disabled> -- </option>
                                <option value="individual">Individual</option>
                                <option value="group">Group</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="code"><?php echo get_phrase('name'); ?></label><span class="required">*</span></label>
                            <input type="text" class="form-control" name="name_add" value="" required>
                        </div>

                        <div class="form-group" id="row_nik">
                            <label for="nik"><?php echo 'NIK'; ?></label><span class="required">*</span></label>
                            <input type="text" class="form-control" name="nik" value="">
                        </div>

                        <div class="form-group">
                            <label for="tahun"><?php echo get_phrase('innovation_year'); ?></label><span class="required">*</span></label>
                            <input id="tahun" type="text" class="form-control" name="tahun" value="<?php echo date('Y'); ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="company"><?php echo get_phrase('company'); ?></label><span class="required">*</span></label>
                            <select class="form-control company" name="company" required>
                                <option value="">&mdash; Choose Company &mdash;</option>
                                <?php foreach ($company as $key => $v) { ?>
                                    <option value="<?php echo $v['id'] ?>"><?php echo $v['company_name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="branch"><?php echo get_phrase('branch'); ?></label>
                            <select name="branch" class="form-control branch" disabled>
                                <option value="">&mdash; No Branch &mdash;</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="title"><?php echo get_phrase('title'); ?></label><span class="required">*</span></label>
                            <input type="text" class="form-control" name="title" value="" required>
                        </div>

                        <div class="form-group">
                            <label for="description"><?php echo get_phrase('description'); ?></label>
                            <textarea cols="80" id="editor-desc" name="description" rows="10"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="code"><?php echo get_phrase('Status'); ?></label><span class="required">*</span></label>
                            <select class="form-control" name="status" id="status" required>
                                <option selected disabled> -- </option>
                                <option value="final-patent">Final Patent</option>
                                <option value="improvement-ongoing">Improvement ongoing</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="code"><?php echo get_phrase('Status'); ?></label><span class="required">*</span></label>
                            <select class="form-control" name="category" id="category" required>
                                <option selected disabled> -- </option>
                                <option value="suggestion_system">Suggestion System</option>
                                <option value="quality_control_circle">Quality Control Circle</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="file-attach"><?php echo get_phrase('upload_dokumen'); ?></label></label><span class="required"> (PDF/DOCX file, max size 2MB)</span></label>
                            <input type="file" id="dokumen" name="dokumen" />
                        </div>
                        <button type="button" class="btn btn-primary" id='tombol-submit' onclick="checkRequiredFields()"><?php echo get_phrase("submit"); ?></button>
                    </form>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<script>
    $(document).ready(function() {
        CKEDITOR.replace('editor-desc');

        $('#row_nik').hide();
        $('#type').on('change', function() {
            if ($('#type').val() == 'individual') {
                $('#row_nik').show();
            } else {
                $('#row_nik').hide();
            }

        });

        $('#dokumen').bind('change', function() {

            //this.files[0].size gets the size of your file.
            // alert(this.files[0].name.split('.').pop().toLowerCase());
            var size = this.files[0].size;
            var ext = this.files[0].name.split('.').pop().toLowerCase();
            if (size > 2097152 || $.inArray(ext, ['docx', 'doc', 'pdf']) == -1) {
                error_size_upload();
                $('#tombol-submit').prop('disabled', true);
            } else {
                $('#tombol-submit').prop('disabled', false);
            }

        });

        $("#tahun").datepicker({
            autoclose: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years"
        });

        $('select.company').change(function(e) {
            var company_id = $(this).val();
            let url = '<?= base_url('admin/get_branch') ?>';
            $.post(url, {
                company_id: company_id
            }, function(data) {
                $('.branch').empty();
                $('.branch').html(data);
                if (company_id == "") {
                    $(this).attr('required', 'required');
                    $('.branch').empty();
                    $('.branch').html('<option value="">&mdash; No Branch &mdash;</option>');
                    $('.branch').attr('disabled', 'disabled');
                } else {
                    var branch_length = $('.branch > option').length;
                    var branch_value = $('.branch > option:first-child').val();
                    if (branch_length == '1' && branch_value == '') {
                        $('.branch').attr('disabled', 'disabled');
                        $('.branch').removeAttr('required');
                    } else {
                        $('.branch').removeAttr('disabled');
                        $('.branch').removeAttr('required');
                    }
                }
            });
        });
    });
</script>