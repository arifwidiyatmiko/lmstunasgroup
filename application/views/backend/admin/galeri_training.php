<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo 'Form Training'; ?> </h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<div class="row justify-content-center">
    <div class="col-xl-12">
        <?php
        if ($this->session->flashdata('error')) {
        ?>
            <div class="alert alert-danger" role="alert">
                <?= $this->session->flashdata('error')['error'] ?>
            </div>
        <?php
        }
        ?>
        <div class="card">
            <div class="card-body">
                <div class="col-lg-12">
                    <div class="dropzone">

                        <div class="dz-message">
                            <h3> Klick or Drop image in here</h3>
                        </div>

                    </div>
                    <a href="<?=base_url('admin/training')?>" class="btn btn-secondary">Kembali</a>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<script>
    // Dropzone.autoDiscover = false;
    // $(document).ready(function() {
    //     $("#dZUpload").dropzone({
    //         maxFilesize: 12,
    //         url: "{{route('media.fileupload')}}",
    //         dictDefaultMessage: 'Drop files to upload <span>or CLICK</span>',
    //         dictFileTooBig: 'Image is larger than 5 MB',
    //         acceptedFiles: ".jpeg,.jpg,.png,.gif,.webp",
    //         addRemoveLinks: true,
    //         timeout: 10000,
    //         // renameFile: function(file) {
    //         //     return file.name = "products_" + file.name;
    //         // },
    //         removedfile: function(file) {
    //             console.log('remove', JSON.parse(file.xhr.response).success);
    //             var name = JSON.parse(file.xhr.response).success;
    //             $.ajax({
    //                 headers: {
    //                     'X-CSRF-TOKEN': "{{ csrf_token() }}",
    //                 },
    //                 type: 'POST',
    //                 url: "{{route('media.fileremove')}}",
    //                 data: {
    //                     filename: name
    //                 },
    //                 success: function(data) {
    //                     console.log("File has been successfully removed!!");
    //                 },
    //                 error: function(e) {
    //                     console.log(e);
    //                 }
    //             });
    //             var _ref;
    //             if (file.previewElement) {
    //                 if ((_ref = file.previewElement) != null) {
    //                     _ref.parentNode.removeChild(file.previewElement);
    //                 }
    //             }
    //             $(":input").filter(function() {
    //                 return this.value == name
    //             }).remove();
    //             return this._updateMaxFilesReachedClass();
    //         },
    //         sending: function(file, xhr, formData) {
    //             formData.append("_token", "{{ csrf_token() }}");
    //         },
    //         success: function(file, response) {
    //             var imgName = response;
    //             console.log(imgName);
    //             console.log('file', file);
    //             file.previewElement.querySelector("img").name = imgName.success;
    //             console.log('qselector', file.previewElement.querySelector("img"));
    //             file.previewElement.classList.add("dz-success");
    //             var hidden = '<input type="hidden" name="media[]" value="' + imgName.success + '" id="id-' + imgName.id + '">';
    //             $('#form-work').append(hidden);

    //         },
    //         error: function(file, response) {
    //             file.previewElement.classList.add("dz-error");
    //         }
    //     });
    // });
</script>