<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo $page_title; ?> <?= $training['title'] ?>
                    <a href="#" data-toggle="modal" data-target="#enrollUser" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i><?php echo get_phrase('Enroll Users'); ?></a>
                </h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-3 header-title"><?php echo count($participant->result_array())."/".$training['max_participant']." ". get_phrase('Participant'); ?></h4>
                <div class="table-responsive-sm mt-4">
                    <table id="basic-datatable" class="table table-striped table-centered mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo get_phrase('Name'); ?></th>
                                <th><?php echo get_phrase('Enrolled_At'); ?></th>
                                <th><?php echo get_phrase('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            // print_r($training->result_array());die();
                            foreach ($participant->result_array() as $key => $val) : ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?= $val['first_name'] . " " . $val['last_name'] ?></td>
                                    <td><?php echo $val['created_at']; ?></td>
                                    <td>
                                        <div class="dropright dropright">
                                            <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="mdi mdi-dots-vertical"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item" href="#" onclick="confirm_modal('<?php echo site_url('admin/participant/'.$training['id'].'/delete/' . $val['id']); ?>');"><?php echo get_phrase('remove'); ?></a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<div class="modal fade" id="enrollUser" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Training Title : <?= $training['title'] ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Date Traning : <b><?= $training['date'] ?></b> </p>
                <p>Current Participant : <b><?= count($participant->result_array()) ?></b> </p>
                <p>Maximum Participant : <b><?= $training['max_participant'] ?></b> </p>
                <form action="<?= base_url('admin/participant/' . $training['id'] . '/new') ?>" id="my-form" method="post">
                    <div class="form-group">
                        <label for="users">Choose Participant <b>You can choose one or more</b></label>
                        <select multiple="multiple" class="form-control js-example-basic-multiple" name="users[]" id="users">
                            <?php
                            function myfunction($v)
                            {
                                return $v['id_users'];
                            }
                            $marks = array_map("myfunction", $participant->result_array());
                            // print_r($marks);die();
                            foreach ($user->result_array() as $key => $value) {
                                if (!in_array($value['id'], $marks)) {
                            ?>
                                    <option value="<?=$value['id']?>"><?= $value['first_name'] . " " . $value['last_name'] . "(" . $value['email'] . ") " ?></option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" form="my-form">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
    });
</script>