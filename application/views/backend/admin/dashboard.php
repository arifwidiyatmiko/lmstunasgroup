<?php
$status_wise_courses = $this->crud_model->get_status_wise_courses();
$number_of_courses = $status_wise_courses['pending']->num_rows() + $status_wise_courses['active']->num_rows();
$number_of_lessons = $this->crud_model->get_lessons()->num_rows();
$number_of_enrolment = $this->crud_model->enrol_history()->num_rows();
$number_of_students = $this->user_model->get_user()->num_rows();
$number_of_training = $this->training_model->get_training()->num_rows();
$ebook = $this->ebook_model->getEbook()->num_rows();
$innovation = $this->innovation_model->count_all();
// $innovation = count($innovation);
$external = $this->external_model->getData();
$external_innovation = $external->num_rows();
$pending = 0;$accepted = 0;$rejected = 0;

foreach ($external->result() as $key => $value) {
    // echo json_encode($value);
    if ($value->status_propose == 'submitted') {
        $pending += 1;
    }elseif ($value->status_propose == 'approved') {
        $accepted += 1;
    }else {
        $rejected += 1;
    }
}
// echo $pending;die();

$company = $this->innovation_model->getCompany()->num_rows();
$branch = $this->innovation_model->getBranch()->num_rows();
$training = $this->training_model->get_training();
$completed_training = 0;
$upcomming_training = 0;
foreach ($training->result() as $key => $value) {
    if (strtotime($value->date) >= strtotime(date('Y-m-d h:i:s'))) {
        $completed_training += 1;
    } else {
        $upcomming_training += 1;
    }
}
?>
<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('dashboard'); ?></h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<!-- <div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title mb-4"><?php echo get_phrase('active_course'); ?></h4>

                <div class="mt-3 chartjs-chart" style="height: 320px;">
                    <canvas id="task-area-chart"></canvas>
                </div>
            </div> 
        </div> 
    </div>
</div> -->

<div class="row">
    <div class="col-12">
        <div class="card widget-inline">
            <div class="card-body p-0">
                <div class="row no-gutters">
                    <div class="col-sm-6 col-xl-4">
                        <a href="<?php echo site_url('admin/courses'); ?>" class="text-secondary">
                            <div class="card shadow-none m-0">
                                <div class="card-body text-center">
                                    <!-- <i class="dripicons-archive text-muted" style="font-size: 24px;"></i> -->
                                    <h3><span><?php echo $number_of_courses; ?></span></h3>
                                    <p class="text-muted font-15 mb-0"><?php echo get_phrase('total_courses'); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-6 col-xl-4">
                        <a href="<?php echo site_url('admin/training'); ?>" class="text-secondary">
                            <div class="card shadow-none m-0 border-left">
                                <div class="card-body text-center">
                                    <!-- <i class="dripicons-camcorder text-muted" style="font-size: 24px;"></i> -->
                                    <h3><span><?php echo $number_of_training; ?></span></h3>
                                    <p class="text-muted font-15 mb-0"><?php echo get_phrase('Total Training'); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-6 col-xl-4">
                        <a href="<?php echo site_url('admin/enrol_history'); ?>" class="text-secondary">
                            <div class="card shadow-none m-0 border-left">
                                <div class="card-body text-center">
                                    <!-- <i class="dripicons-network-3 text-muted" style="font-size: 24px;"></i> -->
                                    <h3><span><?php echo $number_of_enrolment; ?></span></h3>
                                    <p class="text-muted font-15 mb-0"><?php echo get_phrase('number_of_enrolment'); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>

                </div> <!-- end row -->
            </div>
        </div> <!-- end card-box-->
    </div> <!-- end col-->
</div>

<div class="row">
    <div class="col-12">
        <div class="card widget-inline">
            <div class="card-body p-0">
                <div class="row no-gutters">
                    <div class="col-sm-6 col-xl-2">
                        <a href="<?php echo site_url('admin/ebook'); ?>" class="text-secondary">
                            <div class="card shadow-none m-0">
                                <div class="card-body text-center">
                                    <!-- <i class="dripicons-archive text-muted" style="font-size: 24px;"></i> -->
                                    <h3><span><?php echo $ebook; ?></span></h3>
                                    <p class="text-muted font-15 mb-0"><?php echo get_phrase('total_ebooks'); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-6 col-xl-2">
                        <a href="<?php echo site_url('admin/innovation'); ?>" class="text-secondary">
                            <div class="card shadow-none m-0 border-left">
                                <div class="card-body text-center">
                                    <!-- <i class="dripicons-camcorder text-muted" style="font-size: 24px;"></i> -->
                                    <h3><span><?php echo $innovation; ?></span></h3>
                                    <p class="text-muted font-15 mb-0"><?php echo get_phrase('total_innovation'); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-6 col-xl-2">
                        <a href="<?php echo site_url('admin/proposal'); ?>" class="text-secondary">
                            <div class="card shadow-none m-0 border-left">
                                <div class="card-body text-center">
                                    <!-- <i class="dripicons-camcorder text-muted" style="font-size: 24px;"></i> -->
                                    <h3><span><?php echo $external_innovation; ?></span></h3>
                                    <p class="text-muted font-15 mb-0"><?php echo get_phrase('total_propose_training'); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-6 col-xl-2">
                        <a href="<?php echo site_url('admin/company'); ?>" class="text-secondary">
                            <div class="card shadow-none m-0 border-left">
                                <div class="card-body text-center">
                                    <!-- <i class="dripicons-network-3 text-muted" style="font-size: 24px;"></i> -->
                                    <h3><span><?php echo $company; ?></span></h3>
                                    <p class="text-muted font-15 mb-0"><?php echo get_phrase('total_company'); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-6 col-xl-2">
                        <a href="<?php echo site_url('admin/company'); ?>" class="text-secondary">
                            <div class="card shadow-none m-0 border-left">
                                <div class="card-body text-center">
                                    <!-- <i class="dripicons-user-group text-muted" style="font-size: 24px;"></i> -->
                                    <h3><span><?php echo $branch; ?></span></h3>
                                    <p class="text-muted font-15 mb-0"><?php echo get_phrase('total_branch_of_company'); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-xl-2">
                        <a href="<?php echo site_url('admin/users'); ?>" class="text-secondary">
                            <div class="card shadow-none m-0 border-left">
                                <div class="card-body text-center">
                                    <!-- <i class="dripicons-user-group text-muted" style="font-size: 24px;"></i> -->
                                    <h3><span><?php echo $number_of_students; ?></span></h3>
                                    <p class="text-muted font-15 mb-0"><?php echo get_phrase('number_of_users'); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div> <!-- end row -->
            </div>
        </div> <!-- end card-box-->
    </div> <!-- end col-->
</div>
<div class="row">
    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-4"><?php echo get_phrase('course_overview'); ?></h4>
                <div class="my-4 chartjs-chart" style="height: 202px;">
                    <canvas id="project-status-chart"></canvas>
                </div>
                <div class="row text-center mt-2 py-2">
                    <div class="col-6">
                        <i class="mdi mdi-trending-up text-success mt-3 h3"></i>
                        <h3 class="font-weight-normal">
                            <span><?php echo $status_wise_courses['active']->num_rows(); ?></span>
                        </h3>
                        <p class="text-muted mb-0"><?php echo get_phrase('active_courses'); ?></p>
                    </div>
                    <div class="col-6">
                        <i class="mdi mdi-trending-down text-warning mt-3 h3"></i>
                        <h3 class="font-weight-normal">
                            <span><?php echo $status_wise_courses['pending']->num_rows(); ?></span>
                        </h3>
                        <p class="text-muted mb-0"> <?php echo get_phrase('pending_courses'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-4"><?php echo get_phrase('training_overview'); ?></h4>
                <div class="my-4 chartjs-chart" style="height: 202px;">
                    <canvas id="project-status-chart1"></canvas>
                </div>
                <div class="row text-center mt-2 py-2">
                    <div class="col-6">
                        <i class="mdi mdi-trending-up text-warning mt-3 h3"></i>
                        <h3 class="font-weight-normal">
                            <span><?php echo $upcomming_training; ?></span>
                        </h3>
                        <p class="text-muted mb-0"><?php echo get_phrase('upcoming'); ?></p>
                    </div>
                    <div class="col-6">
                        <i class="mdi mdi-trending-down text-success mt-3 h3"></i>
                        <h3 class="font-weight-normal">
                            <span><?php echo $completed_training; ?></span>
                        </h3>
                        <p class="text-muted mb-0"> <?php echo get_phrase('completed'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-4"><?php echo get_phrase('course_overview'); ?></h4>
                <div class="col-sm-12">
                    <!-- <a href="<?php echo site_url('admin/users'); ?>" class="text-secondary"> -->
                    <div class="card shadow-none m-0 border-bottom">
                        <div class="card-body text-center">
                            <!-- <i class="dripicons-user-group text-muted" style="font-size: 24px;"></i> -->
                            <h3><span><?php echo $pending; ?></span></h3>
                            <p class="text-muted font-15 mb-0"><?php echo get_phrase('pending'); ?></p>
                        </div>
                    </div>
                    <!-- </a> -->
                </div>
                <div class="col-sm-12">
                    <!-- <a href="<?php echo site_url('admin/users'); ?>" class="text-secondary"> -->
                    <div class="card shadow-none m-0 border-bottom">
                        <div class="card-body text-center">
                            <!-- <i class="dripicons-user-group text-muted" style="font-size: 24px;"></i> -->
                            <h3><span class="text-success"><?php echo $accepted; ?></span></h3>
                            <p class="text-muted font-15 mb-0"><?php echo get_phrase('accepted'); ?></p>
                        </div>
                    </div>
                    <!-- </a> -->
                </div>
                <div class="col-sm-12">
                    <!-- <a href="<?php echo site_url('admin/users'); ?>" class="text-secondary"> -->
                    <div class="card shadow-none m-0 border-bottom">
                        <div class="card-body text-center">
                            <!-- <i class="dripicons-user-group text-muted" style="font-size: 24px;"></i> -->
                            <h3><span class="text-warning"><?php echo $rejected; ?></span></h3>
                            <p class="text-muted font-15 mb-0"><?php echo get_phrase('rejected'); ?></p>
                        </div>
                    </div>
                    <!-- </a> -->
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#unpaid-instructor-revenue').mouseenter(function() {
        $('#go-to-instructor-revenue').show();
    });
    $('#unpaid-instructor-revenue').mouseleave(function() {
        $('#go-to-instructor-revenue').hide();
    });
</script>