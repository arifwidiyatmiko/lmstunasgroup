<style type="text/css">
  $background1: #c1c1e1;

  html,
  body {
    background-color: $background1;
  }

  table {
    background: white;
  }

  select,
  .select2 {
    display: block;
    width: 100%;
  }

  main {
    margin: 1rem auto;
    max-width: 960px;
  }

  .filters {
    background: darken($background1, 8%);
    border-color: darken($background1, 15%);
  }

  .table-topper {
    background-color: #e1e1e1;

    &:hover,
    &:active {
      background-color: darken(#e1e1e1, 5%);
    }
  }
</style>
<div class="row ">
  <div class="col-xl-12">
    <div class="card">
      <div class="card-body">
        <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo $page_title; ?>
          <div class="btn-group  float-right">
            <button type="button" class="btn btn-outline-primary btn-rounded alignToTitle dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Import Innovation
            </button>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="<?php echo site_url('admin/excel/innovation'); ?>">Unduh Format</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#" data-toggle="modal" data-target="#import_innovation">Import</a>
            </div>
          </div>&nbsp;
          <!-- <a href="<?php echo site_url('admin/innovation/add'); ?>" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i><?php echo get_phrase('add_innovation'); ?></a> -->
          <a href="<?php echo site_url('admin/innovation/add'); ?>" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i><?php echo get_phrase('add_innovation'); ?></a>
        </h4>
      </div> <!-- end card body-->
    </div> <!-- end card -->
  </div><!-- end col-->
</div>

<div class="row">
  <div class="col-xl-12">
    <div class="card">
      <div class="card-body">
        <h4 class="mb-3 header-title"><?php echo get_phrase('innovation'); ?></h4>

        <section class="form-inline">
          <legend class="col-xs-12">Filters</legend>
          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
            <label for="tahun">Tahun</label>
            <select id="tahun" name="tahun">
              <option value=""></option>
              <?php if (!empty($filter['year'])) {
                foreach ($filter['year'] as $key => $v) {
              ?>
                  <option value="<?php echo $v['year'] ?>"><?php echo $v['year'] ?></option>
              <?php
                }
              }
              ?>
            </select>
          </div>

          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
            <label for="company">Company</label>
            <select id="company" name="company">
              <option value=""></option>
              <?php if (!empty($filter['company'])) {
                foreach ($filter['company'] as $key => $v) {
              ?>
                  <option value="<?php echo $v['company_name'] ?>"><?php echo $v['company_name'] ?></option>
              <?php
                }
              }
              ?>
            </select>
          </div>

          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
            <label for="branch">Branch</label>
            <select id="branch" name="branch">
              <option value=""></option>
              <?php if (!empty($filter['branch'])) {
                foreach ($filter['branch'] as $key => $v) {
              ?>
                  <option value="<?php echo $v['company_name'] ?>"><?php echo $v['company_name'] ?></option>
              <?php
                }
              }
              ?>
            </select>
          </div>
        </section>


        <div class="table-responsive-sm mt-4">
          <table id="datatable" class="table table-striped table-centered mb-0">
            <thead>
              <tr>
                <th>#</th>
                <th><?php echo get_phrase('name'); ?></th>
                <th><?php echo get_phrase('type'); ?></th>
                <th><?php echo get_phrase('tahun'); ?></th>
                <th><?php echo get_phrase('company'); ?></th>
                <th><?php echo get_phrase('branch'); ?></th>
                <th><?php echo get_phrase('title'); ?></th>
                <!-- <th><?php echo get_phrase('description'); ?></th> -->
                <th><?php echo get_phrase('submit date'); ?></th>
                <th><?php echo get_phrase('file'); ?></th>
                <th><?php echo get_phrase('actions'); ?></th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($innovation['innovation'] as $key => $i) : ?>
                <tr>
                  <td><?php echo $key + 1; ?></td>
                  <td><?php echo $i['name']; ?></td>
                  <td><?php echo $i['type']; ?></td>
                  <td><?php echo $i['year']; ?></td>
                  <td><?php echo (empty($i['center']) ? $i['branch_name'] : $i['center']); ?></td>
                  <td><?php echo (!empty($i['center']) ? $i['branch_name'] : "-"); ?></td>
                  <td><?php echo $i['title']; ?></td>
                  <!-- <td><?php echo $i['description']; ?></td> -->
                  <td><?php echo $i['add_date']; ?></td>
                  <td><a href="<?= base_url($i['upload_path']) ?>">Unduh</a></td>
                  <td>
                    <div class="dropright dropright">
                      <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="mdi mdi-dots-vertical"></i>
                      </button>
                      <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="<?php echo site_url('admin/innovation/edit/' . $i['id']) ?>"><?php echo get_phrase('edit'); ?></a></li>
                        <li><a class="dropdown-item" href="#" onclick="confirm_modal('<?php echo site_url('admin/innovation/delete/' . $i['id']); ?>');"><?php echo get_phrase('delete'); ?></a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div> <!-- end card body-->
    </div> <!-- end card -->
  </div><!-- end col-->
</div>

<div class="modal fade" id="import_innovation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Import Innovation data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url('admin/import/innovation') ?>" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label for="exampleFormControlFile1">File Format Excel</label>
            <input type="file" class="form-control-file" id="excel" name="excel">
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- <script type="text/javascript">
   $(document).ready(function() {
    $('#datatable').dataTable();
} );
</script> -->
<link href="https://rawgit.com/select2/select2/master/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://rawgit.com/select2/select2/master/dist/js/select2.js"></script>
<script type="text/javascript">
  $(document).ready(function() {

    var dataTable;

    var select2Init = function() {
      $('select').select2({
        //dropdownAutoWidth : true,
        allowClear: true,
        placeholder: "Select Data",
      });
    };

    var dataTableInit = function() {
      dataTable = $('#datatable').dataTable({
        "columnDefs": [{
          "targets": 3,
          "type": 'num',
        }, {
          "targets": 4,
          "type": 'text',
        }, {
          "targets": 5,
          "type": 'text',
        }],
      });
    };

    var dtSearchInit = function() {

      $('#tahun').change(function() {
        dtSearchAction($(this), 3)
      });
      $('#company').change(function() {
        dtSearchAction($(this), 4);
      });
      $('#branch').change(function() {
        dtSearchAction($(this), 5);
      });

    };

    dtSearchAction = function(selector, columnId) {
      var fv = selector.val();
      if ((fv == '') || (fv == null)) {
        dataTable.api().column(columnId).search('', true, false).draw();
      } else {
        dataTable.api().column(columnId).search(fv, true, false).draw();
      }
    };


    $(document).ready(function() {
      select2Init();
      dataTableInit();
      dtSearchInit();
    });

  });
</script>