<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo $page_title; ?>
                    <!-- <a href="<?php echo site_url('admin/training/add_training_form'); ?>" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i><?php echo get_phrase('add_training'); ?></a> -->
                </h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-3 header-title"><?php echo get_phrase('Training'); ?></h4>
                <div class="table-responsive-sm mt-4">
                    <table id="basic-datatable" class="table table-striped table-centered mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo get_phrase('Submitter'); ?></th>
                                <th><?php echo get_phrase('NIK'); ?></th>
                                <th><?php echo get_phrase('File'); ?></th>
                                <th><?php echo get_phrase('status'); ?></th>
                                <th><?php echo get_phrase('Submitted_at'); ?></th>
                                <th><?php echo get_phrase('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            // echo $training->result_array();die();
                            foreach ($external->result_array() as $key => $val) : ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?= $val['first_name'] . " " . $val['last_name'] ?></td>
                                    <td><?= $val['nik']?></td>
                                    <td><a href="<?= base_url('uploads/training/' . $val['path_file']) ?>" target="blank"><?= $val['path_file'] ?></a></td>
                                    <td><?php echo ucfirst($val['status_propose']); ?></td>
                                    <td><?php echo Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $val['created_at'])->format('d M Y h:i'); ?></td>

                                    <td>
                                        <div class="dropright dropright">
                                            <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="mdi mdi-dots-vertical"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item" href="#" onclick="confirm_modal('<?php echo site_url('admin/proposal/approve/' . $val['id_propose']); ?>');"><?php echo get_phrase('approve'); ?></a></li>
                                                <li><a class="dropdown-item" href="#" onclick="confirm_modal('<?php echo site_url('admin/proposal/reject/' . $val['id_propose']); ?>');"><?php echo get_phrase('reject'); ?></a></li>
                                                <li><a class="dropdown-item" href="#" onclick="confirm_modal('<?php echo site_url('admin/proposal/reset/' . $val['id_propose']); ?>');"><?php echo get_phrase('reset'); ?></a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>