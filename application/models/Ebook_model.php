<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ebook_model extends CI_Model {

    var $table = 'ebook'; //nama tabel dari database
    var $column_order = array(null, 'id','name','nik','year','company_id','title','description'); //field yang ada di table user
    var $column_search = array('id','name','nik','year','company_id','title','description'); //field yang diizin untuk pencarian 
    var $order = array('id' => 'desc'); // default order 
    function __construct()
    {
        parent::__construct();
        /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    public function get_ebook($ebook_id = 0) {
        if ($ebook_id > 0) {
            $res = $this->db->select('*')
                            ->from('ebook')
                            ->where('id', $ebook_id)
                            ->get();
            $result['ebook'] = $res->result_array();
        }else{
            $res = $this->db->select('*')
                            ->from('ebook')
                            ->get();
            $result['ebook'] = $res->result_array();
        }
        return $result;
    }
    public function getEbook($ebook_id = 0)
    {
        if ($ebook_id > 0) {
            $res = $this->db->select('*')
                ->from('ebook')
                ->where('id', $ebook_id)
                ->get();
            
        } else {
            $res = $this->db->select('*')
                ->from('ebook')
                ->get();
        }
        return $res;
    }

    public function add_ebook()
    {
        $path = $this->upload_dokumen();
        $thumbnail = $this->upload_thumbnail();
        $data = array(
            'title'          => $this->input->post('title'),
            'thumbnail'     => $thumbnail,
            'description'   => $this->input->post('description'),
            'path'   => $path,
        );
        $this->db->insert('ebook', $data);
        return true;     
    }


    public function getLimited($start, $num)
    {
        $this->db->limit($num, $start);
        return $this->db->get('ebook');
    }

    public function remove_ebook($user_id,$ebook_id)
    {
        $this->db->where(array('user_id'=>$user_id,'ebook_id'=>$ebook_id));
        $this->db->delete('ebook_enrol');
    }

    public function ebook_enroll($idbook)
    {
        $data = array(
            'user_id'=>$this->session->userdata('user_id'),
            'ebook_id'=>$idbook,
            'is_mandatory'=>false,
            'date_added' => time(),
            'last_modified' => time(),
        );
        $this->db->insert('ebook_enrol',$data);
        return $this->db->insert_id();
    }

    public function enrolls($idbook,$iduser)
    {
        $data = array(
            'user_id' => $iduser,
            'ebook_id' => $idbook,
            'is_mandatory' => true,
            'date_added' => time(),
            'last_modified' => time(),
        );
        $this->db->insert('ebook_enrol', $data);
        return $this->db->insert_id();
    }

    public function ebook_library($id_user)
    {
        $this->db->select('ebook.*,ebook_enrol.is_mandatory as status_enrol');
        $this->db->from('ebook');
        $this->db->join('ebook_enrol','ebook_enrol.ebook_id = ebook.id');
        $this->db->where('ebook_enrol.user_id',$id_user);
        return $this->db->get();
    }
    public function ebook_library_id($id_user,$id)
    {
        $this->db->select('ebook.*');
        $this->db->from('ebook');
        $this->db->join('ebook_enrol', 'ebook_enrol.ebook_id = ebook.id');
        $this->db->where('ebook_enrol.user_id', $id_user);
        $this->db->where('ebook_enrol.ebook_id', $id);
        return $this->db->get();
    }

    public function update_ebook()
    {

        $data = array(
            'title'          => $this->input->post('title'),
            'description'   => $this->input->post('description')
        );

        $dokname = $_FILES["dokumen"]["name"];
        $thumbnail = $_FILES["thumbnail"]["name"];
        if(!empty($dokname)){
            $ebook = $this->get_ebook($this->input->post('id'));
            $path_dok = $ebook['ebook'][0]['path'];
            $name_dok = substr($path_dok, strrpos($path_dok, '/') + 1);
            unlink('./uploads/ebook/'.$name_dok);
            
            $path = $this->upload_dokumen();
            $data['path'] = $path;
            // $this->db->where('id', $this->input->post('id'));
            // $this->db->update('ebook', $data);
        }

        if (!empty($thumbnail)) {
            $ebook = $this->get_ebook($this->input->post('id'));
            $path_dok = $ebook['ebook'][0]['path'];
            $name_dok = substr($path_dok, strrpos($path_dok, '/') + 1);
            unlink('./uploads/ebook/' . $name_dok);

            $path1 = $this->upload_thumbnail();
            $data['thumbnail'] = $path1;
            // $this->db->where('id', $this->input->post('id'));
            // $this->db->update('ebook', $data);
        }
        // echo json_encode($this->input->post('id'));die();
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('ebook', $data);
        return true;     
    }

    public function delete_ebook($id){
        $ebook = $this->get_ebook($id);
        $path_dok = $ebook['ebook'][0]['path'];
        $name_dok = substr($path_dok, strrpos($path_dok, '/') + 1);
        unlink('./uploads/ebook/'.$name_dok);
        $this->db->where('id', $id);
        $this->db->delete('ebook');
        return true;
    }

    public function upload_dokumen() {
        $this->load->library('upload');
        $config['upload_path'] = './uploads/ebook';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = 2000;
  
        $dokname = $_FILES["dokumen"]["name"];
        $file_ext = pathinfo($dokname,PATHINFO_EXTENSION);
        $path_name = date('Ymd')."_".rand().".".$file_ext;
        $config['file_name'] = $path_name;
        
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('dokumen')) {
            $page_data['error'] = array('error' => $this->upload->display_errors());
            // print_r($this->upload->display_errors());die();

            redirect($_SERVER['HTTP_REFERER']);
        } else {
            // $data = array('image_metadata' => $this->upload->data());
            $path = "uploads/ebook/".$path_name;
            return $path;        
        }
    }

    public function upload_thumbnail()
    {
        $this->load->library('upload');
        $config['upload_path'] = './uploads/ebook';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 2000;

        $dokname = $_FILES["thumbnail"]["name"];
        $file_ext = pathinfo($dokname, PATHINFO_EXTENSION);
        $path_name = date('Ymd') . "_" . rand() . "." . $file_ext;
        $config['file_name'] = $path_name;

        $this->upload->initialize($config);
        if (!$this->upload->do_upload('thumbnail')) {
            $page_data['error'] = array('error' => $this->upload->display_errors());
            // print_r($this->upload->display_errors());
            // die();
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            // $data = array('image_metadata' => $this->upload->data());
            $path = "uploads/ebook/" . $path_name;
            return $path;
        }
    }

}
