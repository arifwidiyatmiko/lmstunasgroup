<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Participant_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    // public function get_admin_details()
    // {
    //     return $this->db->get_where('trainings', array('role_id' => 1));
    // }

    public function get_participant($training_id = 0)
    {
        $this->db->select('*');
        $this->db->from('training_participant');
        $this->db->join('users', 'users.id = training_participant.id_users');
        // $this->db->join('training', 'training.id = training_participant.id_training');
        if ($training_id > 0) {
            $this->db->where('training_participant.id_training', $training_id);
        }
        return $this->db->get();
        // return $this->db->get('training_participant');
    }

    public function remove_participant($idTraining,$idUsers)
    {
        $this->db->where('id_users', $idUsers);
        $this->db->where('id_training', $idTraining);
        $this->db->delete('training_participant');

        $this->session->set_flashdata('flash_message', get_phrase('training_deleted_successfully'));
    }

    public function get_all_training($training_id = 0)
    {
        if ($training_id > 0) {
            $this->db->where('id', $training_id);
        }
        return $this->db->get('training_participant');
    }

    public function add_participant($idTraining)
    {
        $data = [];
        foreach ($this->input->post('users') as $key => $value) {
            $data[] = array(
                'id_training' => $idTraining,
                'id_users' => $value,
                'status' =>true,
                'created_at' => date('Y-m-d H:i:s'),
            );
        }
        // print_r($data);die();
        $this->db->insert_batch('training_participant', $data);
        $this->session->set_flashdata('flash_message', get_phrase('participant_added_successfully'));
    
    }

    public function enroll($idTraining,$idUser)
    {
        $data = array(
            'id_training' => $idTraining,
            'id_users' => $idUser,
            'status' => true,
            'created_at' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('training_participant',$data);
    }

    public function check_duplication($action = "", $email = "", $training_id = "")
    {
        $duplicate_email_check = $this->db->get_where('trainings', array('email' => $email));

        if ($action == 'on_create') {
            if ($duplicate_email_check->num_rows() > 0) {
                return false;
            } else {
                return true;
            }
        } elseif ($action == 'on_update') {
            if ($duplicate_email_check->num_rows() > 0) {
                if ($duplicate_email_check->row()->id == $training_id) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
    }

    public function edit_training($training_id)
    { // Admin does this editing
        $data = array(
            'title' => html_escape($this->input->post('title')),
            'description' => html_escape($this->input->post('description')),
            'goals' => html_escape($this->input->post('goals')),
            'max_participant' => html_escape($this->input->post('max_participant')),
            'id_category' => $this->input->post('id_category'),
            'date' => Carbon::createFromFormat('d/m/Y', $this->input->post('date'))->format('Y-m-d'),
        );
        // echo json_encode($data);die();
        if($_FILES['thumbnail']['size'] != 0){

            $fileExt = pathinfo($_FILES["thumbnail"]["name"], PATHINFO_EXTENSION);
            $config['upload_path']          = './uploads/training_participant/';
            $config['allowed_types']        = 'gif|jpg|png|webp|jpeg|svg';
            $config['max_size']             = 0;
            $config['max_width']            = 1024;
            $config['max_height']           = 768;
            $config['file_name'] = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/', '', $data['title'])) . '.'. $fileExt;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('thumbnail')) {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('error', $error);
            } else {
                $data['thumbnail']= strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/', '', $data['title'])) . '.'.$fileExt;
                
                $this->session->set_flashdata('flash_message', get_phrase('training_added_successfully'));
            }
        }
        $this->db->where('id',$training_id);
        $this->db->update('training_participant',$data);
    }
    public function delete_training($training_id = "")
    {
        $this->db->where('id', $training_id);
        $this->db->delete('training_participant');
        $this->session->set_flashdata('flash_message', get_phrase('training_deleted_successfully'));
    }

    
}
