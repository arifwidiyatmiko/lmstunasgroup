<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
        /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    public function get_admin_details() {
        return $this->db->get_where('users', array('role_id' => 1));
    }

    public function get_user($user_id = 0) {
        if ($user_id > 0) {
            $this->db->where('id', $user_id);
        }
        $this->db->where('role_id', 2);
        return $this->db->get('users');
    }

    public function get_all_user($user_id = 0) {
        if ($user_id > 0) {
            $this->db->where('id', $user_id);
        }
        return $this->db->get('users');
    }

    public function mandatory_course($id_user)
    {
        $this->db->select('course.*');
        $this->db->from('user_course');
        $this->db->join('course','course.id = user_course.id_course');
        $this->db->where('user_course.id_user', $id_user);
        return $this->db->get();
    }
    
    function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    public function add_user() {
        $validity = $this->check_duplication('on_create', $this->input->post('email'));
        if ($validity == false) {
            $this->session->set_flashdata('error_message', get_phrase('email_duplication'));
        }else {
            $data['nik'] = html_escape($this->input->post('nik'));
            $data['first_name'] = html_escape($this->input->post('first_name'));
            $data['last_name'] = html_escape($this->input->post('last_name'));
            $data['email'] = html_escape($this->input->post('email'));
            $data['password'] = sha1(html_escape($this->input->post('password')));
            $social_link['facebook'] = html_escape($this->input->post('facebook_link'));
            $social_link['twitter'] = html_escape($this->input->post('twitter_link'));
            $social_link['linkedin'] = html_escape($this->input->post('linkedin_link'));
            $data['social_links'] = json_encode($social_link);
            $data['biography'] = $this->input->post('biography');
            $data['parent_id'] = $this->input->post('parent_id');
            $data['occupation'] = $this->input->post('occupation');
            $data['position'] = $this->input->post('position');
            $data['role_id'] = 2;
            $data['date_added'] = strtotime(date("Y-m-d H:i:s"));
            $data['wishlist'] = json_encode(array());
            $data['watch_history'] = json_encode(array());
            $data['status'] = 1;
            // Add paypal keys
            $paypal_info = array();
            // Bawaan nya ini
            // $paypal['production_client_id'] = html_escape($this->input->post('paypal_client_id'));
            $paypal['production_client_id'] = null;
            array_push($paypal_info, $paypal);
            $data['paypal_keys'] = json_encode($paypal_info);
            // Add Stripe keys
            $stripe_info = array();
            $stripe_keys = array(
                'public_live_key' => null,
                'secret_live_key' => null
                //Bawaanya ini
                // 'public_live_key' => html_escape($this->input->post('stripe_public_key')),
                // 'secret_live_key' => html_escape($this->input->post('stripe_secret_key'))
            );
            array_push($stripe_info, $stripe_keys);
            $data['stripe_keys'] = json_encode($stripe_info);

            $this->db->insert('users', $data);
            $user_id = $this->db->insert_id();
            $this->upload_user_image($user_id);
            foreach ($this->input->post('mandatory_course') as $key => $value) {
                $this->db->insert('enrol',array(
                    'user_id'=>$user_id,
                    'course_id'=>$value,
                    'is_mandatory' => true,
                    'date_added' => strtotime(date('D, d-M-Y'))
                ));
            }
            $this->session->set_flashdata('flash_message', get_phrase('user_added_successfully'));
        }
    }

    public function check_duplication($action = "", $email = "", $user_id = "") {
        $duplicate_email_check = $this->db->get_where('users', array('email' => $email));

        if ($action == 'on_create') {
            if ($duplicate_email_check->num_rows() > 0) {
                return false;
            }else {
                return true;
            }
        }elseif ($action == 'on_update') {
            if ($duplicate_email_check->num_rows() > 0) {
                if ($duplicate_email_check->row()->id == $user_id) {
                    return true;
                }else {
                    return false;
                }
            }else {
                return true;
            }
        }
    }

    public function edit_user($user_id = "") { // Admin does this editing
        $validity = $this->check_duplication('on_update', $this->input->post('email'), $user_id);
        if ($validity) {
            if($this->input->post('password') !== '' & $this->input->post('confirm_password') !== '' ){
                if($this->input->post('password') == $this->input->post('confirm_password')){
                    $data['password'] = sha1(html_escape($this->input->post('password')));
                }else{
                    $this->session->set_flashdata('error_message', get_phrase('password isn\'t match'));
                    redirect('admin/user_form/edit_user_form/'.$user_id);
                }
            }
            $data['nik'] = html_escape($this->input->post('nik'));
            $data['first_name'] = html_escape($this->input->post('first_name'));
            $data['last_name'] = html_escape($this->input->post('last_name'));

            if (isset($_POST['email'])) {
                $data['email'] = html_escape($this->input->post('email'));
            }
            $social_link['facebook'] = html_escape($this->input->post('facebook_link'));
            $social_link['twitter'] = html_escape($this->input->post('twitter_link'));
            $social_link['linkedin'] = html_escape($this->input->post('linkedin_link'));
            $data['social_links'] = json_encode($social_link);
            $data['biography'] = $this->input->post('biography');
            $data['title'] = html_escape($this->input->post('title'));
            $data['last_modified'] = strtotime(date("Y-m-d H:i:s"));
            $data['parent_id'] = $this->input->post('parent_id');
            $data['occupation'] = $this->input->post('occupation');
            $data['position'] = $this->input->post('position');
            // Update paypal keys
            $paypal_info = array();
            $paypal['production_client_id'] = null;
            array_push($paypal_info, $paypal);
            $data['paypal_keys'] = json_encode($paypal_info);
            // Update Stripe keys
            $stripe_info = array();
            $stripe_keys = array(
                'public_live_key' => null,
                'secret_live_key' => null
            );
            array_push($stripe_info, $stripe_keys);
            $data['stripe_keys'] = json_encode($stripe_info);

            $this->db->where('id', $user_id);
            $this->db->update('users', $data);
            $this->upload_user_image($user_id);
            // print_r($this->input->post('mandatory_course'));die();
            $this->db->where(array('user_id' => $user_id, 'is_mandatory' => true));
            $this->db->delete('enrol');
            foreach ($this->input->post('mandatory_course') as $key => $value) {
                $this->db->insert('enrol', array(
                    'user_id' => $user_id,
                    'course_id' => $value,
                    'is_mandatory' => true,
                    'date_added' => strtotime(date('D, d-M-Y'))
                ));
            }
            $this->session->set_flashdata('flash_message', get_phrase('user_update_successfully'));
        }else {
            $this->session->set_flashdata('error_message', get_phrase('email_duplication'));
        }

        $this->upload_user_image($user_id);
    }

    public function delete_user($user_id = "") {
        $this->db->where('id', $user_id);
        $this->db->delete('users');
        $this->session->set_flashdata('flash_message', get_phrase('user_deleted_successfully'));
    }

    public function unlock_screen_by_password($password = "") {
        $password = sha1($password);
        return $this->db->get_where('users', array('id' => $this->session->userdata('user_id'), 'password' => $password))->num_rows();
    }

    public function register_user($data) {
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }

    public function my_courses() {
        return $this->db->get_where('enrol', array('user_id' => $this->session->userdata('user_id')));
    }

    public function upload_user_image($user_id) {
        if (isset($_FILES['user_image']) && $_FILES['user_image']['name'] != "") {
            move_uploaded_file($_FILES['user_image']['tmp_name'], 'uploads/user_image/'.$user_id.'.jpg');
            $this->session->set_flashdata('flash_message', get_phrase('user_update_successfully'));
        }
    }

    public function update_account_settings($user_id) {
        $validity = $this->check_duplication('on_update', $this->input->post('email'), $user_id);
        if ($validity) {
            if (!empty($_POST['current_password']) && !empty($_POST['new_password']) && !empty($_POST['confirm_password'])) {
                $user_details = $this->get_user($user_id)->row_array();
                $current_password = $this->input->post('current_password');
                $new_password = $this->input->post('new_password');
                $confirm_password = $this->input->post('confirm_password');
                if ($user_details['password'] == sha1($current_password) && $new_password == $confirm_password) {
                    $data['password'] = sha1($new_password);
                }else {
                    $this->session->set_flashdata('error_message', get_phrase('mismatch_password'));
                    return;
                }
            }
            $data['email'] = html_escape($this->input->post('email'));
            $this->db->where('id', $user_id);
            $this->db->update('users', $data);
            $this->session->set_flashdata('flash_message', get_phrase('updated_successfully'));
        }else {
            $this->session->set_flashdata('error_message', get_phrase('email_duplication'));
        }
    }

    public function change_password($user_id) {
        $data = array();
        if (!empty($_POST['current_password']) && !empty($_POST['new_password']) && !empty($_POST['confirm_password'])) {
            $user_details = $this->get_all_user($user_id)->row_array();
            $current_password = $this->input->post('current_password');
            $new_password = $this->input->post('new_password');
            $confirm_password = $this->input->post('confirm_password');

            if ($user_details['password'] == sha1($current_password) && $new_password == $confirm_password) {
                $data['password'] = sha1($new_password);
            }else {
                $this->session->set_flashdata('error_message', get_phrase('mismatch_password'));
                return;
            }
        }

        $this->db->where('id', $user_id);
        $this->db->update('users', $data);
        $this->session->set_flashdata('flash_message', get_phrase('password_updated'));
    }


    public function get_instructor($id = 0) {
        if ($id > 0) {
            return $this->db->get_all_user($id);
        }else {
            if ($this->check_if_instructor_exists()) {
                $this->db->select('user_id');
                $this->db->distinct('user_id');
                $query_result =  $this->db->get('course');
                $ids = array();
                foreach ($query_result->result_array() as $query) {
                    if ($query['user_id']) {
                        array_push($ids, $query['user_id']);
                    }
                }

                $this->db->where_in('id', $ids);
                return $this->db->get('users')->result_array();
            }
            else {
                return array();
            }
        }
    }

    public function check_if_instructor_exists() {
        $this->db->where('user_id >', 0);
        $result = $this->db->get('course')->num_rows();
        if ($result > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function get_user_image_url($user_id) {

         if (file_exists('uploads/user_image/'.$user_id.'.jpg'))
             return base_url().'uploads/user_image/'.$user_id.'.jpg';
        else
            return base_url().'uploads/user_image/placeholder.png';
    }
    public function get_instructor_list() {
        $query1 = $this->db->get_where('course', array('status' => 'active'))->result_array();
        $instructor_ids = array();
        $query_result = array();
        foreach ($query1 as $row1) {
            if (!in_array($row1['user_id'], $instructor_ids) && $row1['user_id'] != "") {
                array_push($instructor_ids, $row1['user_id']);
            }
        }
        if (count($instructor_ids) > 0) {
            $this->db->where_in('id', $instructor_ids);
            $query_result = $this->db->get('users');
        }else {
            $query_result = $this->get_admin_details();
        }

        return $query_result;
    }

    public function update_instructor_paypal_settings($user_id = '') {
        // Update paypal keys
        $paypal_info = array();
        $paypal['production_client_id'] = html_escape($this->input->post('paypal_client_id'));
        array_push($paypal_info, $paypal);
        $data['paypal_keys'] = json_encode($paypal_info);
        $this->db->where('id', $user_id);
        $this->db->update('users', $data);
    }
    public function update_instructor_stripe_settings($user_id = '') {
        // Update Stripe keys
        $stripe_info = array();
        $stripe_keys = array(
            'public_live_key' => html_escape($this->input->post('stripe_public_key')),
            'secret_live_key' => html_escape($this->input->post('stripe_secret_key'))
        );
        array_push($stripe_info, $stripe_keys);
        $data['stripe_keys'] = json_encode($stripe_info);
        $this->db->where('id', $user_id);
        $this->db->update('users', $data);
    }

    public function reset($user_id, $course_id){
        $user_detail = $this->get_all_user($user_id)->row_array();
        $watch_history_array = json_decode($user_detail['watch_history'], true);

        foreach ($watch_history_array as $key => $v) {
            $lesson_detail = $this->crud_model->get_lessons('lesson', $v['lesson_id'])->row_array();           
            if($lesson_detail['course_id'] == $course_id){
                // print_r($lesson_detail);
                // die();
                $watch_history_array[$key]['progress'] = 0;
            }
        }
        $data['watch_history'] = json_encode($watch_history_array);
        $this->db->where('id', $user_id);
        $this->db->update('users', $data);
        return true;
    }

    public function remove_quiz_history($course_id,$user_id)
    {
        // Ambil id quiz
        // echo $course_id
        $quiz = $this->db->get_where('lesson',array('lesson_type'=>'quiz','course_id'=> $course_id));
        foreach($quiz->result() as $key =>$val){
            $this->db->where(array('quiz_id' => $val->id,'user_id'=>$user_id));
            $this->db->delete('quiz_history');
        }
        // echo json_encode($quiz->num_rows());die();
    }

    public function un_enroll($user_id,$course_id)
    {
        $this->db->where(array('user_id'=>$user_id,'course_id'=>$course_id));
        $this->db->delete('enrol');
    }
    public function get_subordinate($user_id)
    {
        return $this->db->get_where('users',array('parent_id'=>$user_id));
    }
    public function remove_history($user_id, $course_id)
    {

        $user_detail = $this->get_all_user($user_id)->row_array();
        // User's watch history
        $watch_history_array = json_decode($user_detail['watch_history'], true);
        // print_r($watch_history_array);die();
        foreach ($watch_history_array as $key => $v) {
            $lesson_detail = $this->crud_model->get_lessons('lesson', $v['lesson_id'])->row_array();

            if ($lesson_detail['course_id'] == $course_id) {
                // $watch_history_array[$key]['progress'] = 0;
                unset($watch_history_array[$key]);
            }
        }
        // print_r($watch_history_array);
        // die();
        $data['watch_history'] = json_encode($watch_history_array);
        
        $this->db->where('id', $user_id);
        $this->db->update('users', $data);
        return true;
    }

    public function users_check($nik,$email)
    {
        // echo $email;die();
        $this->db->where("(nik='".$nik."' OR email='". $email."')");
        return $this->db->get('users');
    }

    public function branchByName($branch)
    {
        return $this->db->get_where('innovation_company',array('company_name'=>$branch));
    }
    public function getByNIK($nik)
    {
        $query = "SELECT * FROM `users` WHERE nik = '".$nik."'";
        // echo $query;die();
        // $this->db->like('nik', $nik);
        // return $this->db->get('users');
        return $this->db->query($query);
        // return $this->db->get_where('users', array('nik' => $nik));
    }

    public function import_user($array)
    {
        $data = array(
            'nik'=> $array['A'],
            'first_name' => $array['B'],
            // 'status' => $array['c'],
            'email'=> $array['E'],
            'password'=> sha1('lmsPassword'),
            'occupation'=> $array['C'],
            'position'=> $array['D'],
            'title' => $array['H'],
        );
        $data['role_id'] = 2;
        $data['date_added'] = strtotime(date("Y-m-d H:i:s"));
        $data['wishlist'] = json_encode(array());
        $data['watch_history'] = json_encode(array());
        $data['status'] = 1;
        // Add paypal keys
        $paypal_info = array();
        // Bawaan nya ini
        // $paypal['production_client_id'] = html_escape($this->input->post('paypal_client_id'));
        $paypal['production_client_id'] = null;
        array_push($paypal_info, $paypal);
        $data['paypal_keys'] = json_encode($paypal_info);
        // Add Stripe keys
        $stripe_info = array();
        $stripe_keys = array(
            'public_live_key' => null,
            'secret_live_key' => null
        );
        array_push($stripe_info, $stripe_keys);
        $data['stripe_keys'] = json_encode($stripe_info);
        $social_link['facebook'] = '';
        $social_link['twitter'] = '';
        $social_link['linkedin'] = '';
        $data['social_links'] = json_encode($social_link);
        $branch = $this->branchByName($array['F']);
        if($branch->num_rows() != 0){
            $data['id_company'] = $branch->result_array()[0]['id'];
            $this->db->insert('users', $data);
            $id_user = $this->db->insert_id();
            return $id_user;
        }
    }
    public function update_parent($array, $user_id)
    {
        
        $this->db->where('id', $user_id);
        $id_parent = $this->getByNIK($array['G']);
        echo json_encode($id_parent->result());
        // die();
        if($id_parent->num_rows() == 1){
            $id_parent = $id_parent->result()[0]->id;
            $this->db->update('users', array('parent_id' => $id_parent));
        }
    }


    public function un_enroll_all($course_id)
    {
        $this->db->where(array('course_id' => $course_id));
        $this->db->delete('enrol');
    }
}
