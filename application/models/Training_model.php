<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Training_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    // public function get_admin_details()
    // {
    //     return $this->db->get_where('trainings', array('role_id' => 1));
    // }

    public function save_image($idTraining)
    {
        $config['upload_path']   = './uploads/training/';
        $config['allowed_types'] = 'gif|jpg|png|ico|jpeg|svg|webp';
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('userfile')) {

            $token = $this->input->post('token_foto');
            $nama = $this->upload->data('file_name');
            $insert = array('id_training' => $idTraining, 'path' => $nama, 'token' => $token, 'created_at' => date('Y-m-d H:i:s'));
            // print_r($insert);die();
            $this->db->insert('training_image', $insert);
        }
    }
    public function delete_image($idTraining)
    {
        //Ambil token foto
        $token = $this->input->post('token');
        // echo $token;die();
        $where = array('token'=>$token);
        $foto = $this->db->get('training_image',$where);
        
        // print_r($foto->num_rows());die();
         if ($foto->num_rows() > 0) {
            $hasil = $foto->row();
            $nama_foto = $hasil->path;
            $file = FCPATH . 'uploads/training/' . $nama_foto;
            $this->db->delete('training_image', array('token' => $token));
            // echo $file;die();
            if (file_exists($file)) {
                unlink($file);
            }
        }
        echo "{}";
    }
    public function get_images($idTraining)
    {
        // $where = array('id_training' => $idTraining);
        $this->db->where('id_training',$idTraining);
        $foto = $this->db->get('training_image');
        return $foto;
    }
    public function get_training($training_id = 0)
    {
        if ($training_id > 0) {
            $this->db->where('id', $training_id);
        }
        // echo $training_id;die();
        // $this->db->where('role_id', 2);
        return $this->db->get('training');
    }

    public function get_all_training($training_id = 0)
    {
        if ($training_id > 0) {
            $this->db->where('id', $training_id);
        }
        return $this->db->get('training');
    }

    public function getLimited($start,$num)
    {
        $this->db->limit($num, $start);
        return $this->db->get('training');
    }

    public function add_training()
    {
        $data = array(
            'title' => html_escape($this->input->post('title')),
            'description' => html_escape($this->input->post('description')),
            'goals' => html_escape($this->input->post('goals')),
            'max_participant' => html_escape($this->input->post('max_participant')),
            'id_category' => $this->input->post('id_category'),
            'date' => Carbon::createFromFormat('d/m/Y', $this->input->post('date'))->format('Y-m-d'),
        );
        // echo json_encode($data);die();
        if($_FILES['thumbnail']['size'] != 0){

            $fileExt = pathinfo($_FILES["thumbnail"]["name"], PATHINFO_EXTENSION);
            $config['upload_path']          = './uploads/training/';
            $config['allowed_types']        = 'gif|jpg|png|webp|jpeg|svg';
            $config['max_size']             = 0;
            $config['max_width']            = 1024;
            $config['max_height']           = 768;
            $config['file_name'] = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/', '', $data['title'])) . '.' . $fileExt;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('thumbnail')) {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('error', $error);
            } else {
                $data['thumbnail']= strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/', '', $data['title'])).'.'.$fileExt;
                $this->db->insert('training', $data);
                $training_id = $this->db->insert_id();
                $this->session->set_flashdata('flash_message', get_phrase('training_added_successfully'));
            }
        }else {
            $error = array('error' => 'Thumbail is required');
            $this->session->set_flashdata('error', $error);
        }
        // var_dump($_FILES['thumbnail']);die();

    }

    public function get_training_by_user($id_user)
    {
        $this->db->select('*');
        $this->db->from('training_participant');
        $this->db->join('training', 'training.id = training_participant.id_training');
        // $this->db->join('training', 'training.id = training_participant.id_training');
        if ($id_user > 0) {
            $this->db->where('training_participant.id_users', $id_user);
        }
        return $this->db->get();
        // return $this->db->get('training_participant');
    }

    public function get_participant($id)
    {
        // if ($training_id > 0) {
            $this->db->where('id_training', $id);
        // }
        return $this->db->get('training_participant');
    }

    public function check_duplication($action = "", $email = "", $training_id = "")
    {
        $duplicate_email_check = $this->db->get_where('trainings', array('email' => $email));

        if ($action == 'on_create') {
            if ($duplicate_email_check->num_rows() > 0) {
                return false;
            } else {
                return true;
            }
        } elseif ($action == 'on_update') {
            if ($duplicate_email_check->num_rows() > 0) {
                if ($duplicate_email_check->row()->id == $training_id) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
    }

    public function edit_training($training_id)
    { // Admin does this editing
        $data = array(
            'title' => html_escape($this->input->post('title')),
            'description' => html_escape($this->input->post('description')),
            'goals' => html_escape($this->input->post('goals')),
            'max_participant' => html_escape($this->input->post('max_participant')),
            'id_category' => $this->input->post('id_category'),
            'date' => Carbon::createFromFormat('d/m/Y', $this->input->post('date'))->format('Y-m-d'),
        );
        // echo json_encode($data);die();
        if($_FILES['thumbnail']['size'] != 0){

            $fileExt = pathinfo($_FILES["thumbnail"]["name"], PATHINFO_EXTENSION);
            $config['upload_path']          = './uploads/training/';
            $config['allowed_types']        = 'gif|jpg|png|webp|jpeg|svg';
            $config['max_size']             = 0;
            $config['max_width']            = 1024;
            $config['max_height']           = 768;
            $config['file_name'] = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/', '', $data['title'])) . '.'. $fileExt;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('thumbnail')) {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('error', $error);
            } else {
                $data['thumbnail']= strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/', '', $data['title'])) . '.'.$fileExt;
                
                $this->session->set_flashdata('flash_message', get_phrase('training_added_successfully'));
            }
        }
        $this->db->where('id',$training_id);
        $this->db->update('training',$data);
    }
    public function delete_training($training_id = "")
    {
        $this->db->where('id', $training_id);
        $this->db->delete('training');
        $this->session->set_flashdata('flash_message', get_phrase('training_deleted_successfully'));
    }

    
}
