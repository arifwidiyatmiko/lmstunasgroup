<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Innovation_model extends CI_Model {

    var $table = 'innovation'; //nama tabel dari database
    var $column_order = array(null, 'id','name','nik','year','company_id','title','description'); //field yang ada di table user
    var $column_search = array('id','name','nik','year','company_id','title','description'); //field yang diizin untuk pencarian 
    var $order = array('id' => 'desc'); // default order 
    function __construct()
    {
        parent::__construct();
        /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    public function get_innovation($innovation_id = 0) {
        if ($innovation_id > 0) {
            $res = $this->db->select('a.*, b.id as company_id, b.company_name as branch_name, b.id as branch, c.id as center_id, c.company_name as center')
                            ->from('innovation a')
                            ->join('innovation_company b', 'b.id=a.company_id', 'left')
                            ->join('innovation_company c', 'c.id=b.company_center', 'left')
                            ->where('a.id', $innovation_id)
                            ->get();
            $result['innovation'] = $res->result_array();
        }else{
            $res = $this->db->select('a.*, b.id as company_id, b.company_name as branch_name, b.id as branch, c.id as center_id, c.company_name as center')
                            ->from('innovation a')
                            ->join('innovation_company b', 'b.id=a.company_id', 'left')
                            ->join('innovation_company c', 'c.id=b.company_center', 'left')
                            ->get();
            $result['innovation'] = $res->result_array();
        }
        return $result;
    }

    public function checkTitle($arr)
    {
        return $this->db->get_where('innovation',array('title'=>$arr['F']));
    }

    public function getCabang($branch)
    {
        return $this->db->get_where('innovation_company',array('company_name'=>$branch));
    }

    public function import($arr)
    {
        $data = array(
            'name'=>$arr['B'],
            'nik' => $arr['C'],
            'year' => $arr['D'],
            'title' => $arr['F'],
            'description' => $arr['G'],
            'type' => $arr['A'],
            'status' => $arr['H'],
            'category' => $arr['I'],
            'add_date' => date('Y-m-d H:i:s'),
        );
        $branch = $this->getCabang($arr['E']);
        // print_r($branch->result());die();
        if($branch->num_rows() > 0){
            $data['company_id'] = $branch->result()[0]->id;
            $this->db->insert('innovation',$data);
        }
    }

    public function add_innovation()
    {
        $path = $this->upload_dokumen();
        if(!empty($this->input->post('branch'))){
            $company = $this->input->post('branch');
        }else{
            $company = $this->input->post('company');
        }
        $data = array(
            'name'          => $this->input->post('name_add'),
            // 'nik'           => $this->input->post('nik'),
            'year'          => $this->input->post('tahun'),
            'company_id'    => $company,
            'title'         => $this->input->post('title'),
            'description'   => $this->input->post('description'),
            'add_date'      => date('Y-m-d H:i:s'),
            'upload_path'   => $path,
            'status'        => $this->input->post('status'),
            'category'        => $this->input->post('category'),
            'type'          => $this->input->post('type'),
        );
        if ($data['type'] == 'individual') {
            $data['nik'] = $this->input->post('nik');
        }
        $this->db->insert('innovation', $data);
        // $id = $this->db->insert_id();
        // if(!empty($this->input->post('branch'))){
        //     $res = $this->insert_branch($id);
        // }
        return true;     
    }

    public function update_innovation()
    {
        $dokname = $_FILES["dokumen"]["name"];
        if(!empty($dokname)){
            $innovation = $this->get_innovation($this->input->post('id'));
            $path_dok = $innovation['innovation'][0]['upload_path'];
            $name_dok = substr($path_dok, strrpos($path_dok, '/') + 1);
            unlink('./uploads/innovation/'.$name_dok);
            
            $path = $this->upload_dokumen();
            $data['upload_path'] = $path;
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('innovation', $data);
        }
        $branch = $this->input->post('branch');
        if($branch != ''){
            $company = $this->input->post('branch');
        }else{
            $company = $this->input->post('company');
        }
        
        $data = array(
            'name' => $this->input->post('name_add'),
            // 'nik' => $this->input->post('nik'),
            'year'          => $this->input->post('tahun'),
            'company_id' => $company,
            'title' => $this->input->post('title'),
            'description' => $this->input->post('description'),
            'status'        => $this->input->post('status'),
            'type'          => $this->input->post('type'),
            'category'        => $this->input->post('category'),
        );

        if ($data['type'] == 'individual') {
            $data['nik'] = $this->input->post('nik');
        }else {
            $data['nik'] = NULL;
        }
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('innovation', $data);
        return true;     
    }

    public function delete_innovation($id){
        $innovation = $this->get_innovation($id);
        $path_dok = $innovation['innovation'][0]['upload_path'];
        $name_dok = substr($path_dok, strrpos($path_dok, '/') + 1);
        unlink('./uploads/innovation/'.$name_dok);
        $this->db->where('id', $id);
        $this->db->delete('innovation');
        return true;
    }

    public function upload_dokumen() {
        $this->load->library('upload');
        $config['upload_path'] = './uploads/innovation';
        $config['allowed_types'] = 'pdf|doc|docx';
        $config['max_size'] = 2000;
  
        $dokname = $_FILES["dokumen"]["name"];
        $file_ext = pathinfo($dokname,PATHINFO_EXTENSION);
        $path_name = date('Ymd')."_".rand().".".$file_ext;
        $config['file_name'] = $path_name;
        
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('dokumen')) {
            $page_data['error'] = array('error' => $this->upload->display_errors());
            // print_r($this->upload->display_errors());die();
            $page_data['page_name'] = 'innovation_add';
            $page_data['page_title'] = get_phrase('innovation_add');
            $this->load->view('backend/index', $page_data);
        } else {
            $data = array('image_metadata' => $this->upload->data());
            $path = "uploads/innovation/".$path_name;
            return $path;        
        }
    }


    public function getCompany()
    {
        $this->db->where('company_center','0');
        return $this->db->get('innovation_company');
    }
    public function getBranch()
    {
        $this->db->where('company_center !=', '0');
        return $this->db->get('innovation_company');
    }

    public function get_company($company_id=0, $tipe='') {
        if($tipe == 'edit'){
            $this->db->select('*')
                     ->from('innovation_company')
                     ->where('id', $company_id);    
        }else{
            $this->db->select('a.*,b.company_name as center')
                     ->from('innovation_company a')
                     ->join('innovation_company b', 'b.id=a.company_center', 'left');
            if($company_id == 0 || $company_id > 0){
                $this->db->where('a.company_center', $company_id);
            }
        }
        $res = $this->db->get();
        $result = $res->result_array();
        return $result;
    }

    public function add_company()
    {
        $data = array(
            'company_name'          => $this->input->post('company_name'),
            'company_center'           => $this->input->post('company_center'),
        );
        $this->db->insert('innovation_company', $data);
        return true;     
    }

    public function update_company()
    {
        $data = array(
            'company_name'          => $this->input->post('company_name'),
            'company_center'           => $this->input->post('company_center'),
        );
        $this->db->where('id', $this->input->post('company_id'));
        $this->db->update('innovation_company', $data);
        return true;     
    }

    public function delete_company($id){
        $this->db->where('company_center', $id);
        $this->db->delete('innovation_company');
        $this->db->where('id', $id);
        $this->db->delete('innovation_company');
        return true;
    }

    public function get_filter(){
        $res = $this->db->distinct()
                        ->select('year')
                        ->from('innovation')
                        ->get();
        $result['year'] = $res->result_array();
        $res = $this->db->distinct()
                        ->select('company_name')
                        ->from('innovation_company')
                        ->where('company_center', 0)
                        ->get();
        $result['company'] = $res->result_array();
        $res = $this->db->distinct()
                        ->select('company_name')
                        ->from('innovation_company')
                        ->where('company_center !=', 0, FALSE)
                        ->get();
        $result['branch'] = $res->result_array();
        return $result;
    }


    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_branch()
    {
        $branch = $this->db->get('innovation_company');
        foreach($branch->result() as $key){
            $data[]=$key->company_name;
        }
        return $data;
    }
}
