<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class External_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    public function getData()
    {
        $this->db->select('*,training_external.status as status_propose,training_external.id as id_propose');
        $this->db->from('training_external');
        $this->db->join('users','users.id = training_external.id_user');
        return $this->db->get();
    }
    public function getByUser($idUser)
    {
        $this->db->select('*,training_external.status as status_propose,training_external.id as id_propose');
        $this->db->from('training_external');
        $this->db->join('users', 'users.id = training_external.id_user');
        $this->db->where('training_external.id_user',$idUser);
        return $this->db->get();
    }

    public function update($id,$status)
    {
        // echo $id;die();
        // $this->db->where('id', $training_id);
        // $this->db->update('training', $data);
        $this->db->where('id',$id);
        $this->db->update('training_external',array('status'=>$status));
    }

    public function save($idUser)
    {
        // echo json_encode($_FILES["file"]);die();
        $nik = $this->user_model->get_user($idUser)->result()[0]->nik;
        $file_name = pathinfo($_FILES['file']['name'], PATHINFO_FILENAME);
        $data = [
            'id_user' => $idUser,
            'status'=>'submitted',
            'created_at'=>date('Y-m-d H:i:s')
        ];
        if ($_FILES['file']['size'] != 0) {
            $fileExt = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
            // $str = rand();
            $filename = uniqid()."_".$nik."_". slugify($file_name); 
            $config['upload_path']          = './uploads/training/';
            $config['allowed_types']        = 'pdf';
            $config['max_size']             = 0;
            $config['file_name'] = $filename . '.' . $fileExt;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                // print_r($error);die();
                $this->session->set_flashdata('error', $error);
                return false;
            } else {
                $data['path_file'] = $filename . '.' . $fileExt;
                $this->db->insert('training_external', $data);
                $training_id = $this->db->insert_id();
                return true;
            }
        } else {
            $error = array('error' => 'File is required');
            $this->session->set_flashdata('error', $error);
            return false;
        }
    }
}